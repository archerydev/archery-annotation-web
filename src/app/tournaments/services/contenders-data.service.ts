import { Injectable } from '@angular/core';
import { Contender } from '../model/tournament';
import { KuzzleDataService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';


@Injectable()
export class ContendersDataService extends KuzzleDataService<Contender> {
  constructor(kuzzle: KuzzleService) {
    super('Contender', kuzzle);
  }
}
