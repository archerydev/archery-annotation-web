import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PureFileEditComponent } from './pure-file-edit.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { emptyFile } from '../../model/tournament-data';
import { MaterialModule } from 'src/app/material.module';
import { MdcDialogRefFactory } from '../tournaments.stories';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';

const props = {};

const prov = [];

export default {
  title: 'Tournaments|File',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureFileEditComponent],
      imports: [ReactiveFormsModule, MaterialModule, FlexLayoutModule],
      providers: [
        FormBuilder,
        ErrorStateMatcher,
        {
          provide: MdcDialogRef,
          useFactory: MdcDialogRefFactory,
          deps: [MDC_DIALOG_DATA]
        }
      ]
    })
  ]
};

export const New = () => ({
  component: PureFileEditComponent,
  props: {
    ...props
  },
  moduleMetadata: {
    providers: [
      {
        provide: MDC_DIALOG_DATA,
        useValue: {
          file: emptyFile,
          mode: 'create'
        }
      }
    ]
  }
});

New.story = {
  name: 'new'
};

export const Edit = () => ({
  component: PureFileEditComponent,
  props: {
    ...props
  },
  moduleMetadata: {
    providers: [
      {
        provide: MDC_DIALOG_DATA,
        useValue: {
          file: { name: 'test5', url: 'test9' },
          mode: 'update'
        }
      }
    ]
  }
});

Edit.story = {
  name: 'edit'
};
