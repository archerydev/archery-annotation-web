import { Component, OnInit, Input, ChangeDetectionStrategy, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { NgxSubFormComponent, Controls, subformComponentProviders } from 'ngx-sub-form';
import { GeoLocation } from '../../model/club';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Marker, MapOptions, Layer, latLng, tileLayer, marker, icon, LatLng } from 'leaflet';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { PlaceSuggestion, GeocodingFeatureProperties, GeocodingGeometry } from 'src/app/autocomplete/model/geolocation';
import * as L from 'leaflet';
import '../../../../../node_modules/pelias-leaflet-plugin/dist/leaflet-geocoder-mapzen.js';

@Component({
  selector: 'app-geolocation-sub-form',
  templateUrl: './geolocation-sub-form.component.html',
  styleUrls: ['./geolocation-sub-form.component.scss'],
  providers: subformComponentProviders(GeolocationSubFormComponent),
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeolocationSubFormComponent extends NgxSubFormComponent<GeoLocation> implements AfterViewInit {

  constructor(private http: HttpClient, private cd: ChangeDetectorRef) {
    super();
  }
  @Input()
  searchAddressLabel = 'Search Address';

  @Input()
  streetNameLabel = 'Street';

  @Input()
  streetNumberLabel = 'Nr.';

  @Input()
  postalCodeLabel = 'Postal Code';

  @Input()
  localityLabel = 'City';

  @Input()
  regionLabel = 'Province';

  @Input()
  country: string | string[];

  @Input()
  geoLocation: GeoLocation;

  private requestSub: Subscription;
  markers: Layer[] = [];
  center = latLng(40.416775, -3.703790);
  zoom = 5;

  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      })
    ],
    zoom: this.zoom,
    center: this.center
  };
  geocoder: any;
  ngAfterViewInit(): void {
    this.addMarker();
    this.cd.detectChanges();
  }

  addMarker() {
    this.removeMarker();
    if (!this.geoLocation || !this.geoLocation.geoPos) {
      return;
    }
    const newMarker = marker([this.geoLocation?.geoPos[1], this.geoLocation?.geoPos[0]], {
      icon: icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        popupAnchor: [2, -35],
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
      })
    }).bindPopup('<p>' + this.geoLocation?.description + '</p>');
    this.markers.push(newMarker);
    this.center = newMarker.getLatLng();
    this.zoom = 18;
    newMarker.openPopup();
  }

  removeMarker() {
    this.markers.pop();
  }

  selectedItem($event: PlaceSuggestion) {
    console.log('selectedItem', $event);
    this.syncAutoComplete(this.parsePlace($event));
  }

  public writeValue(obj) {
    super.writeValue(obj);
    this.geoLocation = obj;
  }

  syncAutoComplete(geoLocation: GeoLocation) {
    console.log(geoLocation);
    if (this.geoLocation) {
      this.formGroup.reset();
    }
    //TODO: check that the place has an address

    this.geoLocation = geoLocation;

    if (this.geoLocation?.description) {
      this.formGroupControls.description.patchValue(this.geoLocation.description);
    }
    if (this.geoLocation?.streetName) {
      this.formGroupControls.streetName.patchValue(this.geoLocation.streetName);
    }
    if (this.geoLocation?.streetNumber) {
      this.formGroupControls.streetNumber.patchValue(this.geoLocation.streetNumber);
    }
    if (this.geoLocation?.postalCode) {
      this.formGroupControls.postalCode.patchValue(this.geoLocation.postalCode);
    }
    if (this.geoLocation?.region) {
      this.formGroupControls.region.patchValue(this.geoLocation.region);
    }
    if (this.geoLocation?.city) {
      this.formGroupControls.city.patchValue(this.geoLocation.city);
    }
    if (this.geoLocation?.geoPos) {
      this.formGroupControls.geoPos.setValue(this.geoLocation.geoPos);
      this.addMarker();
    }
  }

  parsePlace(place: PlaceSuggestion): GeoLocation {
    console.log(place);
    if (place.data.confidence < 0.8) {
      return {
        description: `LatLong: ${place.geometry.coordinates[1]}, ${place.geometry.coordinates[0]} near ${place.data.label}`,
        geoPos: place.geometry.coordinates,
        streetName: null,
        streetNumber: null,
        postalCode: null,
        city: null,
        region: null
      };
    }
    return {
      description: place.data.label,
      geoPos: place.geometry.coordinates,
      streetName: place.data.street,
      streetNumber: place.data.housenumber,
      postalCode: place.data.postalcode,
      city: place.data.localadmin ?? place.data.locality,
      region: place.data.region
    };
  }
  onMapClick(event: any) {
    console.log(event);
    this.generateSuggestions(event.latlng);
    this.geocoder.reset();
  }

  private generateSuggestions(point: LatLng) {
    // const YOUR_API_KEY = '';
    const url = `https://pelias.gpulido.com/v1/reverse?point.lat=${point.lat}&point.lon=${point.lng}&size=1`;

    if (this.requestSub) {
      this.requestSub.unsubscribe();
    }
    this.requestSub = this.http.get(url).subscribe(
      (data: any) => {
        console.log(data);
        const placeSuggestions = data.features.map(feature => this.getPlaceSuggestion(feature));
        let ps = null;
        if (placeSuggestions.length) {
          ps = { ...placeSuggestions[0] };
          ps.geometry.coordinates = [point.lng, point.lat];
        }
        const geo = this.parsePlace(ps);
        this.syncAutoComplete(geo);
      },
      err => {
        console.log(err);
      }
    );
  }
  getPlaceSuggestion(feature: any) {
    const properties: GeocodingFeatureProperties = feature.properties as GeocodingFeatureProperties;
    const geometry: GeocodingGeometry = feature.geometry as GeocodingGeometry;
    return {
      fullAddress: properties.label,
      data: properties,
      geometry
    };
  }

  onMapReady(map: L.Map) {
    // Do stuff with map
    const options = {
      url: 'https://pelias.gpulido.com/v1',
      params: { lang: 'es', country: 'ES' },
      attribution: 'Geocoding by <a href="https://pelias.gpulido.com">Pelias</a>',
      //fullWidth: 650,
      expanded: true,
      textStrings: {
        INPUT_PLACEHOLDER: 'Search',
        INPUT_TITLE_ATTRIBUTE: 'Search',
        RESET_TITLE_ATTRIBUTE: 'Reset',
        NO_RESULTS: 'No results were found.'
      }
      // markers: {
      //   icon: icon({
      //     iconSize: [25, 41],
      //     iconAnchor: [13, 41],
      //     popupAnchor: [2, -35],
      //     iconUrl: 'assets/marker-icon.png',
      //     shadowUrl: 'assets/marker-shadow.png'
      //   }),
      // }
    };
    this.geocoder = L.control.geocoder(options).addTo(map);
    const geocoderEl = this.geocoder._container;
    geocoderEl.parentNode.insertBefore(geocoderEl, geocoderEl.parentNode.childNodes[0]);
    this.geocoder.focus();
    this.geocoder.on('select', e => {
      const place = this.getPlaceSuggestion(e.feature);
      this.syncAutoComplete(this.parsePlace(place));
    });
  }

  protected getFormControls(): Controls<GeoLocation> {
    return {
      description: new FormControl(null, Validators.required),
      geoPos: new FormControl(null),
      streetName: new FormControl(null),
      streetNumber: new FormControl(null),
      postalCode: new FormControl(null),
      city: new FormControl(null),
      region: new FormControl(null)
    };
  }

  public getDefaultValues(): Partial<GeoLocation> | null {
    return {
      description: null,
      geoPos: null,
      streetName: null,
      streetNumber: null,
      postalCode: null,
      city: null,
      region: null
    };
  }

  protected transformToFormGroup(obj: GeoLocation | null, defaultValues: Partial<GeoLocation> | null): GeoLocation | null {
    console.log('GeoLoc transformToFormGroup', obj);
    if (!obj) {
      return null;
    }
    console.log(defaultValues);
    const geoLoc = {
      ...defaultValues,
      ...obj
    } as GeoLocation;
    console.log(geoLoc);
    return geoLoc;
  }
}
