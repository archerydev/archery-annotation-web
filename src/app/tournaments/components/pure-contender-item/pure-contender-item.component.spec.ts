import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureContenderItemComponent } from './pure-contender-item.component';

describe('PureContenderItemComponent', () => {
  let component: PureContenderItemComponent;
  let fixture: ComponentFixture<PureContenderItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureContenderItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureContenderItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
