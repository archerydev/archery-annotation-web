import { addons } from '@storybook/addons';
import { create } from '@storybook/theming/create';
//import brandImage from 'logo.svg';

const theme = create({
  base: 'light',
  brandTitle: 'Archery Annotation',
  brandImage:'/logo.svg'
});

addons.setConfig({
  panelPosition: 'bottom',
  theme,
});
