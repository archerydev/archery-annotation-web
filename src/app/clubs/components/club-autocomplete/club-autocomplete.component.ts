import {
  Component,
  Input,
  forwardRef,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  ViewChild,
  Injector,
  AfterViewInit
} from '@angular/core';
import { Club } from 'src/app/clubs/model/club';
import { Subject } from 'rxjs';
import { MdcAutocompleteComponent } from '../../../autocomplete/mdc-autocomplete/mdc-autocomplete.component';
import { provideValueAccessor, FormControlSuperclass } from 's-ng-utils';

@Component({
  selector: 'club-autocomplete',
  templateUrl: './club-autocomplete.component.html',
  styleUrls: ['./club-autocomplete.component.scss'],
  providers: [provideValueAccessor(ClubAutocompleteComponent)],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClubAutocompleteComponent extends FormControlSuperclass<Club> implements AfterViewInit {
  // tslint:disable-next-line:variable-name
  @ViewChild(MdcAutocompleteComponent, { static: false }) _autocomplete!: MdcAutocompleteComponent<Club>;

  @Input() item: Club;

  // tslint:disable-next-line:variable-name
  private _items: Club[];
  @Input() set items(value: Club[]) {
    this._items = value;
    this.onFieldChange(this.item?.name);
  }
  get items(): Club[] {
    return this._items;
  }

  isDisabled: boolean;
  filteredClubs: Subject<Club[]> = new Subject<Club[]>();
  filteredFn = (arg0: Club) => arg0.name;

  constructor(injector: Injector) {
    super(injector);
  }

  onSelectItem(value: Club) {
    this.emitOutgoingValue(value);
  }

  ngAfterViewInit() {
    this._autocomplete.writeValue(this.item);
    this.onFieldChange(this.item?.name);
    this._autocomplete.hideOptions();
  }

  handleIncomingValue(value: Club): void {
    this.item = value;
  }

  onFieldChange(value: string) {
    this.filteredClubs.next(this.getFilteredItems(value));
  }

  onEnterPress(value: string) {
    const fItems = this.getFilteredItems(value);
    if (fItems.length === 1) {
      this.onSelectItem(fItems[0]);
      this._autocomplete.hideOptions();
    }
  }

  getFilteredItems(value: string): Club[] {
    let fItems = this.items;
    if (value && value !== '') {
      fItems = this.items.filter(item =>
        this.filteredFn(item)
          .toUpperCase()
          .includes(value.toUpperCase() ?? '')
      );
    }
    return fItems;
  }
  clearQuery() {
    this.item = null;
    this._autocomplete.writeValue(null);
    this.emitOutgoingValue(null);
  }
}
