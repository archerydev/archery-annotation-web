import { Component } from '@angular/core';
import { KuzzleService } from 'kuzzle-ngrx';
import { KuzzleFilesService } from 'kuzzle-ngrx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'archery-annotation';

  constructor(private kuzzleService: KuzzleService, private kuzzleFilesService: KuzzleFilesService) {}
}
