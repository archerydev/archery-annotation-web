import { Component, OnInit } from '@angular/core';
import { TournamentEntityService } from '../../services/tournament-entity.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Tournament, Contender } from '../../model/tournament';
import { map, tap, withLatestFrom } from 'rxjs/operators';
import { ContenderEntityService } from '../../services/contender-entity.service';
import { MdcDialog, MdcDialogConfig } from '@angular-mdc/web/dialog';
import { ContenderEditComponent } from '../contender-edit/contender-edit.component';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss']
})
export class TournamentComponent implements OnInit {
  tournament$: Observable<Tournament>;

  loading$: Observable<boolean>;
  contenders$: Observable<Contender[]>;
  nextPage = 0;

  constructor(private tournamentsService: TournamentEntityService,
              private contendersService: ContenderEntityService,
              public dialog: MdcDialog,
              private route: ActivatedRoute) {}

  ngOnInit() {

    const tournamentId = this.route.snapshot.paramMap.get('id');
    this.tournament$ = this.tournamentsService.entities$
      .pipe(
        map(tournaments => tournaments.find(tournament => tournament.id === tournamentId))
      );

    this.contenders$ = this.contendersService.entities$
    .pipe(
      withLatestFrom(this.tournament$),
      tap(([contenders, tournament]) => {
        if (this.nextPage === 0) {
          this.loadContendersPage(tournament);
        }
      }),
      map(([contenders, tournament]) => contenders.filter(contender => contender.tournamentId === tournament.id))
    );
    this.loading$ = this.contendersService.loading$;
  }

  loadContendersPage(tournament: Tournament) {
    this.contendersService.getWithQuery({
      tournamentId: tournament.id,
      pageSize: '3',
      pageNumber: '0'
    });
    this.nextPage += 1;
  }

  onRegister(tournament: Tournament) {

    const dialogConfig = new MdcDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      mode: 'create',
      tournament
    };

    const dialogRef = this.dialog.open(ContenderEditComponent, dialogConfig);

  }

}
