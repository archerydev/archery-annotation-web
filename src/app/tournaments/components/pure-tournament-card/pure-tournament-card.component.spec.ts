import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureTournamentCardComponent } from './pure-tournament-card.component';
import { MaterialModule } from 'src/app/material.module';
import { Tournament } from '../../model/tournament';
import { setupTournaments } from '../../model/tournament-data';

export const defaultTournaments: Tournament[] = setupTournaments();
export const tournament = defaultTournaments[0];

describe('TournamentItemComponent', () => {
  let component: PureTournamentCardComponent;
  let fixture: ComponentFixture<PureTournamentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureTournamentCardComponent ],
      imports: [MaterialModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureTournamentCardComponent);

    component = fixture.componentInstance;
    component.tournament = tournament;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
