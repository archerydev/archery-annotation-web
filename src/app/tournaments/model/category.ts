import { IdModel } from 'kuzzle-ngrx';

export interface Category extends IdModel {
  division: string;
  archeryClass: string;
}

export const categoryMapping = {
  dynamic: 'strict',
  _meta: {
    modelVersion: '1'
  },
  properties: {
    division: { type: 'text' },
    archeryClass: { type: 'text' }
  }
};

export const categoryModuleMappings = {
  Category: categoryMapping
};
