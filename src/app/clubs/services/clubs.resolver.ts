import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap, filter, first } from 'rxjs/operators';
import { ClubEntityService } from './club-entity.service';


@Injectable()
export class ClubsResolver implements Resolve<boolean> {
  constructor(private clubsService: ClubEntityService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.clubsService.loaded$
    .pipe(
      tap(loaded => {
        if (!loaded) {
          this.clubsService.getAll();
        }
      }),
      filter(loaded => !!loaded),
      first()
    );
   }
}
