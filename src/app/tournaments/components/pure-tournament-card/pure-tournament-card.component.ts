import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Tournament } from '../../model/tournament';

@Component({
  selector: 'app-pure-tournament-card',
  templateUrl: './pure-tournament-card.component.html',
  styleUrls: ['./pure-tournament-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureTournamentCardComponent implements OnInit {
  @Input() tournament: Tournament;
  @Output() viewTournament: EventEmitter<any> = new EventEmitter();
  @Output() deleteTournament: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onView(id) {
    this.viewTournament.emit(id);
  }

  onDelete(id) {
    this.deleteTournament.emit(id);
  }
}
