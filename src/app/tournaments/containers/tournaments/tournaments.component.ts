import { Component, OnInit } from '@angular/core';
import { TournamentEntityService } from '../../services/tournament-entity.service';
import { Observable } from 'rxjs/internal/Observable';
import { Tournament } from '../../model/tournament';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.scss']
})
export class TournamentsComponent implements OnInit {
  constructor(private tournamentsService: TournamentEntityService,
              private router: Router
              ) {}

  activeTournaments$: Observable<Tournament[]>;
  loading$: Observable<boolean>;

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.activeTournaments$ = this.tournamentsService.entities$;
    this.loading$ = this.tournamentsService.loading$;
  }

  onViewTournament(event: any) {
    console.log(event);
    this.router.navigate(['tournaments', event]);
  }
}
