import { Injectable } from '@angular/core';
import { EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Category } from '../model/category';
import { KuzzleRealtimeEntityService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';


@Injectable()
export class CategoryEntityService extends KuzzleRealtimeEntityService<Category> {
  constructor(kuzzle: KuzzleService, serviceElementsFactory: EntityCollectionServiceElementsFactory) {

    super('Category', kuzzle, serviceElementsFactory);
  }
}


