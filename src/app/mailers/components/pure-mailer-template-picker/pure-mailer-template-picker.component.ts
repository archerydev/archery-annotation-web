import { Component, OnInit, Input, EventEmitter, Output, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MailerTemplate } from '../../model/mailer';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';

@Component({
  selector: 'app-pure-mailer-template-picker',
  templateUrl: './pure-mailer-template-picker.component.html',
  styleUrls: ['./pure-mailer-template-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureMailerTemplatePickerComponent implements OnInit {
  templates: MailerTemplate[];
  loading = false;

  constructor(public dialogRef: MdcDialogRef<PureMailerTemplatePickerComponent>, @Inject(MDC_DIALOG_DATA) data) {
    this.templates = data.templates;
  }

  ngOnInit() {}

  onSelectTemplate(template: MailerTemplate) {
    this.dialogRef.close(template);
  }
}
