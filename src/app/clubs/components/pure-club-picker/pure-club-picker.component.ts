import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewContainerRef,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { Club } from '../../model/club';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';

@Component({
  selector: 'app-pure-club-picker',
  templateUrl: './pure-club-picker.component.html',
  styleUrls: ['./pure-club-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureClubPickerComponent implements OnInit {
  clubs: Club[];
  loading = false;
  selectedClub: Club;
  // @Input() selectedClub: Club = {
  //   id: '-1',
  //   name: null
  // };
  //@Output() selectClub: EventEmitter<any> = new EventEmitter();

  @ViewChild('template', { static: true }) template;

  constructor(public dialogRef: MdcDialogRef<PureClubPickerComponent>, @Inject(MDC_DIALOG_DATA) data) {
    (this.clubs = data.clubs), (this.loading = data.loading), (this.selectedClub = data.selectedClub);
  }

  ngOnInit() {}

  onSelectClub(club: Club) {
    this.dialogRef.close(club);
  }

  isSelected(club: Club): boolean {
    return this.selectedClub && this.selectedClub.id === club.id;
  }
}
