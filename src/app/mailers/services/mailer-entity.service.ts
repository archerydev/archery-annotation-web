import { Injectable } from '@angular/core';
import { EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Mailer } from '../model/mailer';
import { KuzzleRealtimeEntityService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';

@Injectable()
export class MailerEntityService extends KuzzleRealtimeEntityService<Mailer> {
  constructor(kuzzle: KuzzleService, serviceElementsFactory: EntityCollectionServiceElementsFactory) {

    super('Mailer', kuzzle, serviceElementsFactory);
  }
}


