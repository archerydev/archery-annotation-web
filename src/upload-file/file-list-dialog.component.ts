import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UploadFileDialogComponent } from './upload-file-dialog.component';
import { MdcDialogRef, MdcDialog, MdcDialogConfig } from '@angular-mdc/web/dialog';
import { KuzzleFilesService } from 'kuzzle-ngrx';

@Component({
  templateUrl: 'file-list-dialog.component.html',
  styleUrls: ['file-list-dialog.component.scss']
})
export class FileListDialogComponent implements OnInit {
  filesPath$: Observable<string[]>;

  constructor(
    public dialogRef: MdcDialogRef<FileListDialogComponent>,
    public dialog: MdcDialog,
    public kuzzleFilesService: KuzzleFilesService
  ) {}

  ngOnInit() {
    this.filesPath$ = this.kuzzleFilesService.getFilesPath();
  }

  upload() {
    const dialogConfig = new MdcDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      mode: 'singleFile',
      uploadDir: 'archeryannotation'
    };
    const dref = this.dialog.open(UploadFileDialogComponent, dialogConfig);
    dref.afterClosed().subscribe(result => {
      console.log(result);
      if (result !== null && result !== '') {
        this.dialogRef.close(result as { file_path: string });
      }
    });
  }

  selectFile(filepath: string) {
    this.dialogRef.close({ file_path: filepath });
  }
}
