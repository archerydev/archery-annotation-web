import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { PureTournamentComponent } from './pure-tournament.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { tournament, tournamentNoLoc, tourActions } from '../tournaments.stories';
import { HttpClientModule } from '@angular/common/http';

const props = {
  register: tourActions.register,
};

export default {
  title: 'Tournaments/Tournament',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureTournamentComponent],
      imports: [MaterialModule, FlexLayoutModule, LeafletModule, HttpClientModule],
    }),
  ],
};

export const Default = () => ({
  component: PureTournamentComponent,
  props: {
    ...props,
    tournament: object('tournament', tournament),
  },
});

Default.story = {
  name: 'default',
};

export const NoPos = () => ({
  component: PureTournamentComponent,
  props: {
    ...props,
    tournament: object('tournament', tournamentNoLoc),
  },
});

NoPos.story = {
  name: 'no-pos',
};
