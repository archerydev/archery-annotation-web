import { Tournament, Contender } from '../model/tournament';
import { setupTournaments, setupContenders } from '../model/tournament-data';
import { action } from '@storybook/addon-actions';
import { Club } from '../../clubs/model/club';
import { setupClubs } from '../../clubs/model/club-data';
import { Category } from '../model/category';
import { setupCategories } from '../model/category-data';
import { MailerTemplate, Mailer } from '../../mailers/model/mailer';
import { setupMailerTemplates, setupMailers } from '../../mailers/model/mailer-data';
import { MdcDialogConfig } from '@angular-mdc/web/dialog';

export const categories: Category[] = setupCategories();
export const defaultTournaments: Tournament[] = setupTournaments();
export const tournament = defaultTournaments[1];
export const defaultMailers: Mailer[] = setupMailers();
export const tournamentNoLoc = defaultTournaments[2];
export const defaultClubs: Club[] = setupClubs();
export const club = defaultClubs[1];
export const defaultContenders: Contender[] = setupContenders();
export const contender = defaultContenders[0];
export const defaultMailerTemplates: MailerTemplate[] = setupMailerTemplates();

export const tourActions = {
  viewTournament: action('viewTournament'),
  deleteTournament: action('deleteTournament'),
  register: action('register')
};

export const contenderActions = {
  saveContender: action('saveContender'),
  deleteContender: action('deleteContender'),
};


export const clubActions = {
  saveClub: action('saveClub'),
  deleteClub: action('deleteClub'),
  selectImage: action('selectImage'),
  closeDialog: action('closeDialog'),
  selectClub: action('selectClub')
};


export const MdcDialogRefFactory = depHolder => {
  const dialogConfig = new MdcDialogConfig();
  dialogConfig.data = { ...depHolder.data };

  return {
    _portalInstance: {
      _config: dialogConfig
    },
    close: (dialogResult: any) => {},
    opened: () => {}
  };
};
