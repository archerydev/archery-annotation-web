import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureClubItemComponent } from './pure-club-item.component';

describe('PureClubItemComponent', () => {
  let component: PureClubItemComponent;
  let fixture: ComponentFixture<PureClubItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureClubItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureClubItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
