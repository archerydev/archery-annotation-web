import { Component, OnInit, Input } from '@angular/core';
import { Contender } from '../../model/tournament';

@Component({
  selector: 'app-pure-contenders',
  templateUrl: './pure-contenders.component.html',
  styleUrls: ['./pure-contenders.component.scss']
})
export class PureContendersComponent implements OnInit {

  @Input() contenders: Contender[];

  constructor() { }

  ngOnInit() {
  }

}
