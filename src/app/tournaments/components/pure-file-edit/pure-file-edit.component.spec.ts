import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureFileEditComponent } from './pure-file-edit.component';

describe('PureFileEditComponent', () => {
  let component: PureFileEditComponent;
  let fixture: ComponentFixture<PureFileEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureFileEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureFileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
