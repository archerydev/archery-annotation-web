import { Component, OnInit, Inject } from '@angular/core';
import { Tournament, Contender, emptyContender } from '../../model/tournament';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';
import { ContenderEntityService } from '../../services/contender-entity.service';
import { Club } from '../../../clubs/model/club';

import { PureClubPickerComponent } from '../../../clubs/components/pure-club-picker/pure-club-picker.component';
import { ClubEntityService } from '../../../clubs/services/club-entity.service';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-contender-edit',
  templateUrl: './contender-edit.component.html',
  styleUrls: ['./contender-edit.component.scss']
})
export class ContenderEditComponent implements OnInit {
  contenderId: string;
  contender$: Observable<Contender>;
  tournament: Tournament;
  tournamentId: string;
  tournament$: Observable<Tournament>;

  clubs$: Observable<Club[]>;
  mode: 'create' | 'update';

  constructor(private route: ActivatedRoute,
              private contendersService: ContenderEntityService,
              public clubService: ClubEntityService,
              ) {
                this.clubs$ = clubService.entities$;
                //this.tournament = data.tournament;
              }

  ngOnInit() {
    this.clubService.getAll();
    this.route.params.subscribe((params: Params) => {
      console.log(params);
      this.contenderId = params.contenderid;
      if (params.contenderid != null) {
        this.mode = 'update';
      } else {
        this.mode = 'create';
      }
      this.initForm();
    });
  }

  private initForm() {
    if (this.mode === 'create') {
      this.contender$ = of(emptyContender);
    } else {
      this.contender$ = this.contendersService.entities$
      .pipe(
        map(contenders => contenders.find(contender => contender.id === this.contenderId))
      );
      //TODO: zip with the tournament information

      // this.contenders$ = this.contendersService.entities$
      // .pipe(
      //   withLatestFrom(this.tournament$),
      //   tap(([contenders, tournament]) => {
      //     if (this.nextPage === 0) {
      //       this.loadContendersPage(tournament);
      //     }
      //   }),
      //   map(([contenders, tournament]) => contenders.filter(contender => contender.tournamentId === tournament.id))
      // );
      //   this.tournamentService.
      // }
    }

  }

  onCancel() {
  }

  onSaveContender(contenderToSave: Contender) {
    console.log('save contender');
    // this.contender = { ...this.contender, ...contenderToSave as Contender };
    // console.log(this.contender);
    // if (this.mode === 'create') {
    //   this.contenderService.add(this.contender).subscribe(c => {
    //     this.contender = c;
    //    }
    //     );
    // } else {
    //   this.contenderService.upsert(this.contender).subscribe(c => {
    //     this.contender = c;
    //    }
    //     );
    // }


  }
}
