import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-pure-location-picker',
  templateUrl: './pure-location-picker.component.html',
  styleUrls: ['./pure-location-picker.component.scss']
})
export class PureLocationPickerComponent implements OnInit {

  form: FormGroup;
  constructor() { }

  ngOnInit() {
  }

}
