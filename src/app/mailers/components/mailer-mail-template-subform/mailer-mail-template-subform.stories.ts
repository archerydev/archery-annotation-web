import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { action } from '@storybook/addon-actions';
import { MailerMailTemplateSubFormComponent } from './mailer-mail-template-subform.component';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
    ngModelChange: action('ngModelChange'),
};


export default {
  title: 'Mailers/SubForms/Mail Template',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [MailerMailTemplateSubFormComponent],
      imports: [
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
      ],
      providers: [FormBuilder, ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: MailerMailTemplateSubFormComponent,
  props: {
    ngModel: null,
    ...props,
  }
});

NewEmpty.story = {
  name: 'new empty'
};

// export const Edit = () => ({
//   component: GeolocationSubFormComponent,
//   props: {
//     ngModel: { null
//     },
//     ...props,
//   }
// });

// Edit.story = {
//   name: 'edit'
// };
