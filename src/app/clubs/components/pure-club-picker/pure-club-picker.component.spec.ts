import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureClubPickerComponent } from './pure-club-picker.component';

describe('PureClubPickerComponent', () => {
  let component: PureClubPickerComponent;
  let fixture: ComponentFixture<PureClubPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureClubPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureClubPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
