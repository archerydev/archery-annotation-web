import { Injectable } from '@angular/core';
import { EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Contender } from '../model/tournament';
import { KuzzleRealtimeEntityService } from 'kuzzle-ngrx/';
import { KuzzleService } from 'kuzzle-ngrx';


@Injectable()
export class ContenderEntityService extends KuzzleRealtimeEntityService<Contender>{
  constructor(kuzzle: KuzzleService, serviceElementsFactory: EntityCollectionServiceElementsFactory) {

    super('Contender', kuzzle, serviceElementsFactory);
  }
}


