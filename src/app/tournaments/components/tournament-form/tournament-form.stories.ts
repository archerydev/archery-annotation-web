import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { tournament, categories, defaultClubs } from '../tournaments.stories';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { action } from '@storybook/addon-actions';
import { GeolocationSubFormComponent } from '../../../clubs/components/geolocation-sub-form/geolocation-sub-form.component';
import { AutocompleteModule } from 'src/app/autocomplete/autocomplete.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { HttpClientModule } from '@angular/common/http';
import { TournamentFormComponent } from './tournament-form.component';
import { emptyTournament } from '../../model/tournament';
import { QuillModule } from 'ngx-quill';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
  dataOutput: action('updatedTournament'),
  ngModelChange: action('ngModelChange'),
  allCategories: object('allCategories', categories),
  clubs: object('clubs', defaultClubs)
};

export default {
  title: 'Tournaments/Form',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [TournamentFormComponent, GeolocationSubFormComponent],
      imports: [
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        LeafletModule,
        HttpClientModule,
        QuillModule.forRoot(),
        AutocompleteModule
      ],
      providers: [FormBuilder, ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: TournamentFormComponent,
  props: {
    ...props,
    dataInput: object('dataInput', emptyTournament)
  }
});

NewEmpty.story = {
  name: 'new empty'
};

export const Edit = () => ({
  component: TournamentFormComponent,
  props: {
    ...props,
    dataInput: object('dataInput', tournament)
  }
});

Edit.story = {
  name: 'edit'
};
