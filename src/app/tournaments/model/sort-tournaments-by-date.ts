import {Tournament} from './tournament';


export function sortTournamentsByDate(c1: Tournament, c2: Tournament) {
  return c1.date.getMilliseconds() - c2.date.getMilliseconds();
}
