import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import {  MdcDialogModule } from '@angular-mdc/web/dialog';
import { MdcButtonModule } from '@angular-mdc/web/button';
import { ErrorStateMatcher} from '@angular-mdc/web/form-field';
import { MdcFilePickerComponent } from './mdc-file-picker.component';

export default {
  title: 'Components/File Picker',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [MdcFilePickerComponent],
      imports: [MdcButtonModule, MdcDialogModule],
      providers: [ErrorStateMatcher],
    }),
  ],
};

export const NewEmpty = () => ({
  component: MdcFilePickerComponent,
  props: {
    // items: object('items', defaultClubs),
    // item: object('item', club),
    // propertySelector: object('propertySelector', 'name')
  }
 });

NewEmpty.story = {
  name: 'new empty',
};

export const Edit = () => ({
  component: MdcFilePickerComponent,
  props: {
    item: object('item', 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_colladomediano.png'),
  },
});

Edit.story = {
  name: 'edit',
};

