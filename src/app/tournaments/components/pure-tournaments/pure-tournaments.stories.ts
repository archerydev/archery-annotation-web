import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { PureTournamentsComponent } from './pure-tournaments.component';
import { PureTournamentsCardListComponent } from '../pure-tournaments-card-list/pure-tournaments-card-list.component';
import { PureTournamentCardComponent } from '../pure-tournament-card/pure-tournament-card.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { defaultTournaments, tourActions } from '../tournaments.stories';

export const tournaments = defaultTournaments;

const props = {
  viewTournament: tourActions.viewTournament,
};

export default {
  title: 'Tournaments/List',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [
        PureTournamentsComponent,
        PureTournamentsCardListComponent,
        PureTournamentCardComponent,
      ],
      imports: [MaterialModule, FlexLayoutModule],
      providers: [],
    }),
  ],

  excludeStories: ['tournaments'],
};

export const Default = () => ({
  component: PureTournamentsComponent,
  props: {
    ...props,
    tournaments: object('tournaments', tournaments),
    loading: false,
  },
});

Default.story = {
  name: 'default',
};
