import { Component, OnInit, Input } from '@angular/core';
import { Contender } from '../../model/tournament';

@Component({
  selector: 'app-pure-contender-item',
  templateUrl: './pure-contender-item.component.html',
  styleUrls: ['./pure-contender-item.component.scss']
})
export class PureContenderItemComponent implements OnInit {

  @Input() contender: Contender;
  
  constructor() { }

  ngOnInit() {
  }

}
