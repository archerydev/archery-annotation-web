import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdcLocationAutocompleteComponent } from './mdc-location-autocomplete.component';

describe('MdcLocationAutocompleteComponent', () => {
  let component: MdcLocationAutocompleteComponent;
  let fixture: ComponentFixture<MdcLocationAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdcLocationAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdcLocationAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
