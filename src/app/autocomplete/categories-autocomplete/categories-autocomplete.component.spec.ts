import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesAutocompleteComponent } from './categories-autocomplete.component';

describe('CategoriesAutocompleteComponent', () => {
  let component: CategoriesAutocompleteComponent;
  let fixture: ComponentFixture<CategoriesAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
