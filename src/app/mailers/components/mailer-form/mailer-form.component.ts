import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { NgxRootFormComponent, DataInput, Controls } from 'ngx-sub-form';
import { Mailer, MailerTemplate, MailTemplate, Subscription, DataSource } from '../../model/mailer';
import { FormControl, Validators } from '@angular/forms';
import { defaultMailerTemplates } from '../../../tournaments/components/tournaments.stories';
import { MdcDialog, MdcDialogConfig } from '@angular-mdc/web/dialog';
import { PureMailerTemplatePickerComponent } from '../pure-mailer-template-picker/pure-mailer-template-picker.component';
import * as _ from 'lodash';

export interface MailerForm {
  id: string;
  description: string;
  masterCollection: string; //Name of the collection for which this mailer is associated
  masterId: string; //Master object id for the mailer
  mailTemplate: MailTemplate;
  subscription: Subscription;
  datasources: DataSource[];
  _kuzzle_info: any;
}

@Component({
  selector: 'app-mailer-form',
  templateUrl: './mailer-form.component.html',
  styleUrls: ['./mailer-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MailerFormComponent extends NgxRootFormComponent<Mailer, MailerForm> {
  // tslint:disable-next-line:no-input-rename
  @DataInput()
  @Input('mailer')
  public dataInput: Required<Mailer> | null | undefined;

  @Input() collections: string[];
  @Input() actions: string[];
  @Input() types: string[];

  // tslint:disable-next-line:no-output-rename
  @Output('mailerUpdated') public dataOutput: EventEmitter<Mailer> = new EventEmitter();

  constructor(private dialog: MdcDialog) {
    super();
  }
  protected getFormControls(): Controls<Mailer> {
    return {
      id: new FormControl(null),
      masterCollection: new FormControl(null, Validators.required),
      masterId: new FormControl(null, Validators.required),
      description: new FormControl(null),
      subscription: new FormControl(null),
      mailTemplate: new FormControl(null),
      datasources: new FormControl(null, Validators.required),
      // eslint-disable-next-line @typescript-eslint/camelcase
      _kuzzle_info: new FormControl(null)
    };
  }

  public getDefaultValues(): Partial<MailerForm> | null {
    return {
      id: null,
      masterCollection: null,
      masterId: null,
      description: null,
      subscription: null,
      mailTemplate: null,
      datasources: null,
      // eslint-disable-next-line @typescript-eslint/camelcase
      _kuzzle_info: null
    };
  }

  protected transformToFormGroup(obj: Mailer | null, defaultValues: Partial<MailerForm> | null): MailerForm | null {
    if (!obj) {
      return null;
    }
    return {
      ...defaultValues,
      ...obj,

    } as MailerForm;
  }
  //  subcription: {
  //       ...obj.subscription,
  //       filters: this.prettify(obj.subscription.filters)
  //     }
 // filters: this.prettify(mailer.subscription.filters):
  prettify(text: string): string {
    console.log(text);
    const obj2 = JSON.parse(_.unescape(text));
    return JSON.stringify(obj2, undefined, 4);
  }

  onSelectTemplate() {
    const dialogConfig = new MdcDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      templates: defaultMailerTemplates
    };
    const dialogRef = this.dialog.open(PureMailerTemplatePickerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      const { ['id']: dummy, ...mailerTemplate } = result;
      this.formGroup.patchValue({ ...mailerTemplate });
    });
  }
}
