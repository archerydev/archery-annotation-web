import { Injectable } from '@angular/core';
import { Tournament } from '../model/tournament';
import { KuzzleDataService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';

@Injectable()
export class TournamentsDataService extends KuzzleDataService<Tournament> {
  constructor(kuzzle: KuzzleService) {
    super('Tournament', kuzzle);
  }
}
