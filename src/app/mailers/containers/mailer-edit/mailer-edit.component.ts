import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MailerEntityService } from '../../services/mailer-entity.service';
import { Observable, of } from 'rxjs';
import { NullableObject } from 'ngx-sub-form';
import { Mailer } from '../../model/mailer';
import { map, switchMap, tap, catchError } from 'rxjs/operators';
import { Actions, Collections, Types } from '../../model/mailer-data';
import { TournamentEntityService } from '../../../tournaments/services/tournament-entity.service';
import { UIService } from 'src/app/shared/ui.service';

@Component({
  selector: 'app-mailer-edit',
  templateUrl: './mailer-edit.component.html',
  styleUrls: ['./mailer-edit.component.scss']
})
export class MailerEditComponent {
  // TODO: the tournament service is hardcoded as right now we only
  // create mailers for tournaments. It is needed to be abstracted.
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private mailerService: MailerEntityService,
    private uiService: UIService
  ) {}
  public collections = Collections;
  public actions = Actions;
  public types = Types;

  public mailer$: Observable<NullableObject<Mailer>> = this.route.paramMap.pipe(
    map(params => params.get('mailerId')),
    switchMap(mailerId => {
      if (mailerId === 'new' || !mailerId) {
        return of(null);
      }
      return this.mailerService.getByKey(mailerId);
    }),
    map(mailerId => (mailerId ? mailerId : this.emptyMailer()))
  );

  private emptyMailer(): NullableObject<Mailer> {
    return {
      id: null,
      masterCollection: this.route.snapshot.queryParamMap.get('masterCollection'),
      masterId: this.route.snapshot.queryParamMap.get('masterId'),
      description: null,
      subscription: null,
      mailTemplate: null,
      datasources: null
    };
  }
  onSaveMailer(mailer: Mailer): void {
    this.mailer$ = (mailer.id ? this.mailerService.update(mailer) : this.mailerService.add(mailer)).pipe(
      tap(c => this.uiService.showSnackbar('Saved mailer', null)),
      tap(c => {
        if (mailer.id) {
          this.router.navigate([`../../${c.id}`, 'edit'], { relativeTo: this.route });
        } else {
          this.router.navigate([`../${c.id}`, 'edit'], { relativeTo: this.route });
        }
      }),
      catchError(e => {
        this.uiService.showSnackbar(e, null);
        return this.mailer$;
      })
    );
  }
}
