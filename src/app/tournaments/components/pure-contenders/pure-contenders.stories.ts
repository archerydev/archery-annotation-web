import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { defaultContenders } from '../tournaments.stories';
import { PureContendersComponent } from './pure-contenders.component';
import { AutocompleteModule } from 'src/app/autocomplete/autocomplete.module';

export const contenders = defaultContenders;

const props = {
  //viewTournament: actions.viewTournament
};

export default {
  title: 'Contenders/List',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureContendersComponent],
      imports: [MaterialModule, FlexLayoutModule, AutocompleteModule],
      providers: [],
    }),
  ],

  excludeStories: ['contenders'],
};

export const Default = () => ({
  component: PureContendersComponent,
  props: {
    ...props,
    contenders: object('contenders', contenders),
    loading: false,
  },
});

Default.story = {
  name: 'default',
};
