import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { PureContenderEditComponent } from './pure-contender-edit.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { tournament, contender, contenderActions, clubActions, defaultClubs } from '../tournaments.stories';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PureClubPickerComponent } from '../../../clubs/components/pure-club-picker/pure-club-picker.component';
import { AutocompleteModule } from 'src/app/autocomplete/autocomplete.module';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcDialog } from '@angular-mdc/web/dialog';

const props = {
  saveContender: contenderActions.saveContender,
  selectClub: clubActions.selectClub,
  clubs: defaultClubs
};

export default {
  title: 'Contenders/Edit',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureContenderEditComponent, PureClubPickerComponent],
      imports: [ReactiveFormsModule, MaterialModule, FlexLayoutModule, BrowserAnimationsModule, AutocompleteModule],
      providers: [FormBuilder, ErrorStateMatcher, MdcDialog],
      entryComponents: [PureClubPickerComponent]
    })
  ]
};

export const New = () => ({
  component: PureContenderEditComponent,
  props: {
    ...props,
    mode: 'create',
    tournament: object('tournament', tournament)
  }
});

New.story = {
  name: 'new'
};

export const Edit = () => ({
  component: PureContenderEditComponent,
  props: {
    ...props,
    mode: 'update',
    tournament: object('tournament', tournament),
    contender: object('contender', contender)
  }
});

Edit.story = {
  name: 'edit'
};
