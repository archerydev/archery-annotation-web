import { Injectable } from '@angular/core';
import { Club } from '../model/club';
import { KuzzleDataService, KuzzleService } from 'kuzzle-ngrx';



@Injectable()
export class ClubsDataService extends KuzzleDataService<Club> {
  constructor(kuzzle: KuzzleService) {
    super('Club', kuzzle);
  }
}
