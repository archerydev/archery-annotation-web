import { Component, OnInit } from '@angular/core';
import { Tournament, emptyTournament } from '../../model/tournament';
import { TournamentEntityService } from '../../services/tournament-entity.service';
import { ActivatedRoute, Params, RouterLink, Router } from '@angular/router';
import { of, Observable } from 'rxjs';
import { map, tap, catchError, switchMap, filter } from 'rxjs/operators';
import { Category } from '../../model/category';
import { Club } from '../../../clubs/model/club';
import { CategoryEntityService } from '../../services/category-entity.service';
import { ClubEntityService } from '../../../clubs/services/club-entity.service';
import { MdcDialog } from '@angular-mdc/web/dialog';
import { UIService } from 'src/app/shared/ui.service';
import { NullableObject } from 'ngx-sub-form';
import { Mailer, MailerDesc } from '../../../mailers/model/mailer';

@Component({
  selector: 'app-tournament-edit',
  templateUrl: './tournament-edit.component.html',
  styleUrls: ['./tournament-edit.component.scss']
})
export class TournamentEditComponent implements OnInit {
  categories$: Observable<Category[]> = this.categoriesService.entities$;
  clubs$: Observable<Club[]> = this.clubsService.entities$;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MdcDialog,
    private tournamentsService: TournamentEntityService,
    private categoriesService: CategoryEntityService,
    private clubsService: ClubEntityService,
    private uiService: UIService
  ) {
    this.clubs$ = clubsService.entities$;
    this.categories$ = categoriesService.entities$;
  }

  public tournament$: Observable<NullableObject<Tournament>> = this.route.paramMap.pipe(
    map(params => params.get('id')),
    switchMap(id => {
      if (id === 'new' || !id) {
        return of(null);
      }
      return (this.tournament$ = this.tournamentsService.entities$.pipe(
        map(tournaments => tournaments.find(tournament => tournament.id === id))
      ));

      // return this.tournamentsService.getByKey(id);
    }),
    map(tour => (tour ? tour : emptyTournament))
  );

  ngOnInit() {
    this.clubsService.getAll();
    this.categoriesService.getAll();
  }

  onSaveTournament(tournament: Tournament) {
    this.tournament$ = (tournament.id
      ? this.tournamentsService.update(tournament)
      : this.tournamentsService.add(tournament)
    ).pipe(
      tap(c => this.uiService.showSnackbar('Saved tournament', null)),
      tap(c => {
        if (tournament.id) {
          this.router.navigate([`../../${c.id}`, 'edit'], { relativeTo: this.route });
        } else {
          this.router.navigate([`../${c.id}`, 'edit'], { relativeTo: this.route });
        }
      }),
      catchError(e => {
        this.uiService.showSnackbar(e, null);
        return this.tournament$;
      })
    );
  }

  onAddMailer() {
    console.log('add mailer');
    this.route.paramMap.pipe(map(params => params.get('id'))).subscribe(id =>
      this.router.navigate(['/mailers', 'new'], {
        relativeTo: this.route,
        queryParams: { masterCollection: 'Tournament', masterId: id }
      })
    );
  }

  onEditMailer(mailer: MailerDesc) {
    this.router.navigate(['/mailers', mailer.id], { relativeTo: this.route });

    // this.route.paramMap.pipe(map(params => params.get('id'))).subscribe(id =>
    //   this.router.navigate(['/tournaments/mailers/new'], {
    //     queryParams: { masterCollection: 'Tournament', masterId: id }
    //   })
    // );
  }
}
