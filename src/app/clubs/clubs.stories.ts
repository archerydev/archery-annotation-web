import { action } from '@storybook/addon-actions';


import { MdcDialogConfig } from '@angular-mdc/web/dialog';
import { setupClubs } from './model/club-data';
import { Club } from './model/club';

export const defaultClubs: Club[] = setupClubs();
export const club = defaultClubs[1];

export const clubActions = {
  saveClub: action('saveClub'),
  deleteClub: action('deleteClub'),
  selectImage: action('selectImage'),
  closeDialog: action('closeDialog'),
  selectClub: action('selectClub')
};


export const MdcDialogRefFactory = depHolder => {
  const dialogConfig = new MdcDialogConfig();
  dialogConfig.data = { ...depHolder.data };

  return {
    _portalInstance: {
      _config: dialogConfig
    },
    close: (dialogResult: any) => {},
    opened: () => {}
  };
};
