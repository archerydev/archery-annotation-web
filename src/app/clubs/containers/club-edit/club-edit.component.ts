import { Component, OnInit, Inject } from '@angular/core';
import { Club } from '../../model/club';
import { ClubEntityService } from '../../services/club-entity.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NullableObject } from 'ngx-sub-form';
import { map, switchMap, tap, catchError } from 'rxjs/operators';
import { UIService } from 'src/app/shared/ui.service';

@Component({
  selector: 'app-club-edit',
  templateUrl: './club-edit.component.html',
  styleUrls: ['./club-edit.component.scss']
})
export class ClubEditComponent {
  mode: 'create' | 'update';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private clubService: ClubEntityService,
    private uiService: UIService
  ) {}

  public club$: Observable<NullableObject<Club>> = this.route.paramMap.pipe(
    map(params => params.get('clubId')),
    switchMap(clubId => {
      if (clubId === 'new' || !clubId) {
        return of(null);
      }
      return this.clubService.getByKey(clubId);
    }),
    map(club => (club ? club : this.emptyClub()))
  );

  private emptyClub(): NullableObject<Club> {
    return {
      id: null,
      name: null,
      mail: null,
      imageUrl: null,
      location: null
    };
  }

  // onDeleteClub(club: Club) {
  //   console.log('onDelete');
  //   this.clubService.delete(club.id);
  // }

  onSaveClub(club: Club) {
    console.log('saveclub', club);

    this.club$ = (club.id ? this.clubService.update(club) : this.clubService.add(club)).pipe(
      tap(c => this.router.navigate([`../${c.id}`], { relativeTo: this.route })),
      catchError(e => {
        this.uiService.showSnackbar(e, null);
        return this.club$;
      })
    );

  }
}
