import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureClubsComponent } from './pure-clubs.component';

describe('PureClubsComponent', () => {
  let component: PureClubsComponent;
  let fixture: ComponentFixture<PureClubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureClubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureClubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
