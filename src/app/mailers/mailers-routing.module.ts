import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MailersResolver } from './services/mailers.resolver';
import { MailersComponent } from './containers/mailers/mailers.component';
import { MailerEditComponent } from './containers/mailer-edit/mailer-edit.component';

const routes: Routes = [
  {
    path: '',
    component: MailersComponent,
    resolve: {
      mailers: MailersResolver
    }
  },
  { path: ':mailerId', component: MailerEditComponent },
  { path: 'new', component: MailerEditComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailersRoutingModule {}
