import { EventEmitter, Input, Component, Output, DebugElement, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgxRootFormComponent, Controls, DataInput } from 'ngx-sub-form';
import { Club, GeoLocation, emptyLocation } from '../../model/club';
import { MdcDialog } from '@angular-mdc/web/dialog';
import { FileListDialogComponent } from 'src/upload-file/file-list-dialog.component';
import { mimeType } from '../../../tournaments/model/mime-type.validator';

export interface ClubForm {
  id: string;
  name: string;
  imageUrl: string;
  mail: string;
  location: GeoLocation | null;
  _kuzzle_info: any;
}

@Component({
  selector: 'app-club-form',
  templateUrl: './club-form.component.html',
  styleUrls: ['./club-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClubFormComponent extends NgxRootFormComponent<Club, ClubForm> {
  // tslint:disable-next-line:no-input-rename
  @DataInput()
  @Input('club')
  public dataInput: Required<Club> | null | undefined;

    // tslint:disable-next-line:no-output-rename
  @Output('clubUpdated')
  public dataOutput: EventEmitter<Club> = new EventEmitter();

  protected getFormControls(): Controls<Club> {
    return {
      id: new FormControl(null),
      name: new FormControl(null, Validators.required),
      mail: new FormControl(null, Validators.email),
      imageUrl: new FormControl(null),
      location: new FormControl(null),
      _kuzzle_info: new FormControl(null)
    };
  }

  public getDefaultValues(): Partial<ClubForm> | null {
    return {
      id: null,
      name: null,
      imageUrl: null,
      mail: null,
      location: null,
      _kuzzle_info: null
    };
  }

  protected transformToFormGroup(obj: Club | null, defaultValues: Partial<ClubForm> | null): ClubForm | null {
    if (!obj) {
      return null;
    }
    console.log(obj);
    const clubForm = {
      ...defaultValues,
      ...obj
    } as ClubForm;
    return clubForm;
  }

  public onChangeLocation($event) {
    console.log($event);
    if ($event.checked) {
      this.formGroupControls.location.setValue({ ...emptyLocation });
    } else {
      this.formGroupControls.location.setValue(null);
    }
  }
}
