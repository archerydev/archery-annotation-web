import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { emptyClub, Club } from '../../model/club';
import { ClubFormComponent } from './club-form.component';
import { action } from '@storybook/addon-actions';
import { GeolocationSubFormComponent } from '../geolocation-sub-form/geolocation-sub-form.component';
import { AutocompleteModule } from 'src/app/autocomplete/autocomplete.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { HttpClientModule } from '@angular/common/http';
import { club } from '../../clubs.stories';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
    dataOutput: action('updatedClub'),
    selectImage: action('selectImage'),
    ngModelChange: action('ngModelChange')
   //saveClub: clubActions.saveClub
  // deleteClub: clubActions.deleteClub,
  // selectImage: clubActions.selectImage,
  // closeDialog: clubActions.closeDialog,
};

const testClub: Club  = { id: '1',
    name: 'A.D. Collado Mediano',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_colladomediano.png',
    mail: '',
    location: {
      description: 'Mi lugar',
      geoPos: null,
      streetName: null,
      streetNumber: null,
      postalCode: null,
      city: null,
      region: null
    }
}

export default {
  title: 'Clubs/Form',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [ClubFormComponent, GeolocationSubFormComponent],
      imports: [ReactiveFormsModule, MaterialModule, FlexLayoutModule, LeafletModule, HttpClientModule, AutocompleteModule],
      providers: [FormBuilder, ErrorStateMatcher, MdcIconRegistry],
    }),
  ],
};

export const NewEmpty = () => ({
  component: ClubFormComponent,
  props: {
    ...props,
    mode: 'create',
    dataInput: object('dataInput', emptyClub)
   // ngModel: emptyClub
  },
});

NewEmpty.story = {
  name: 'new empty',
};

export const NewWithData = () => ({
  component: ClubFormComponent,
  props: {
    ...props,
    mode: 'create',
    ngModel: testClub
  },
});

NewWithData.story = {
  name: 'new with data',
};

export const Edit = () => ({
  component: ClubFormComponent,
  props: {
    ...props,
    mode: 'update',
    dataInput: object('club', club),
  },
});

Edit.story = {
  name: 'edit',
};
