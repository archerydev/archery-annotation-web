import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap, filter, first } from 'rxjs/operators';
import { MailerEntityService } from './mailer-entity.service';


@Injectable()
export class MailersResolver implements Resolve<boolean> {
  constructor(private mailersService: MailerEntityService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.mailersService.loaded$.pipe(
      tap(loaded => {
        if (!loaded) {
          this.mailersService.getAll();
        }
      }),
      filter(loaded => !!loaded),
      first()
    );
   }
}
