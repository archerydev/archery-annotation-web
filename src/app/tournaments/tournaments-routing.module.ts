import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TournamentsComponent } from './containers/tournaments/tournaments.component';
import { TournamentsResolver } from './services/tournaments.resolver';
import { TournamentComponent } from './containers/tournament/tournament.component';
import { TournamentEditComponent } from './containers/tournament-edit/tournament-edit.component';
import { ContenderEditComponent } from './containers/contender-edit/contender-edit.component';
import { ClubEditComponent } from '../clubs/containers/club-edit/club-edit.component';
import { MailerEditComponent } from '../mailers/containers/mailer-edit/mailer-edit.component';
import { AdminComponent } from './containers/admin/admin.component';
import { ClubsResolver } from '../clubs/services/clubs.resolver';

const routes: Routes = [
  {
    path: '',
    component: TournamentsComponent,
    resolve: {
      tournaments: TournamentsResolver
    }
  },
  {
    path: 'admin',
    component: AdminComponent,
    pathMatch: 'full'
  },
  {
    path: 'new',
    component: TournamentEditComponent
    // resolve: {
    //   clubs: ClubsResolver
    // }
  },
  {
    path: ':id',
    component: TournamentComponent,
    resolve: {
      tournaments: TournamentsResolver
    }
  },
  {
    path: ':id/edit',
    component: TournamentEditComponent,
    // resolve: {
    //   tournaments: TournamentsResolver
    // }
  },
  {
    path: ':id/contenders/new',
    component: ContenderEditComponent,
    // resolve: {
    //   tournaments: TournamentsResolver
    // }
  },
  {
    path: 'contenders/:contenderid/edit',
    component: ContenderEditComponent,
    // resolve: {
    //   tournaments: TournamentsResolver
    // }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TournamentsRoutingModule {}
