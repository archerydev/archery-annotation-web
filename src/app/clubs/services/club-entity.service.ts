import { Injectable } from '@angular/core';
import { EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Club } from '../model/club';
import { KuzzleRealtimeEntityService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';

@Injectable({
  providedIn: 'root'
})
export class ClubEntityService extends KuzzleRealtimeEntityService<Club> {
  constructor(kuzzle: KuzzleService, serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Club', kuzzle, serviceElementsFactory);
  }
}
