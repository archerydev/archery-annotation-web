import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdcAutocompleteComponent } from './mdc-autocomplete/mdc-autocomplete.component';
import { MdcMenuModule } from '@angular-mdc/web/menu';
import { MdcTextFieldModule } from '@angular-mdc/web/textfield';
import { MdcListModule } from '@angular-mdc/web/list';
import { MdcIconModule } from '@angular-mdc/web/icon';
import { MdcButtonModule } from '@angular-mdc/web/button';
import { MdcDialogModule } from '@angular-mdc/web/dialog';
import { MdcLocationAutocompleteComponent } from './mdc-location-autocomplete/mdc-location-autocomplete.component';
import { HttpClientModule } from '@angular/common/http';
import { CategoriesAutocompleteComponent } from './categories-autocomplete/categories-autocomplete.component';
import { MdcSearchLocationAutocompleteComponent } from './mdc-search-location-autocomplete/mdc-search-location-autocomplete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MdcFilePickerComponent } from './mdc-file-picker/mdc-file-picker.component';
import { MdcChipsModule } from '@angular-mdc/web/chips';

@NgModule({
  declarations: [
    MdcAutocompleteComponent,
    MdcLocationAutocompleteComponent,
    CategoriesAutocompleteComponent,
    MdcSearchLocationAutocompleteComponent,
    MdcFilePickerComponent
  ],
  exports: [
    MdcAutocompleteComponent,
    MdcLocationAutocompleteComponent,
    CategoriesAutocompleteComponent,
    MdcSearchLocationAutocompleteComponent,
    MdcFilePickerComponent,
    CommonModule
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MdcMenuModule,
    MdcTextFieldModule,
    MdcListModule,
    MdcChipsModule,
    MdcIconModule,
    MdcButtonModule,
    HttpClientModule,
    ReactiveFormsModule,
    MdcDialogModule,
    LeafletModule
  ]
})
export class AutocompleteModule {}
