import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Tournament } from '../../model/tournament';

@Component({
  selector: 'app-pure-tournaments',
  templateUrl: './pure-tournaments.component.html',
  styleUrls: ['./pure-tournaments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureTournamentsComponent implements OnInit {
  @Input() tournaments: Tournament[] = [];

  @Input() isLoading = false;
  @Output() viewTournament: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
