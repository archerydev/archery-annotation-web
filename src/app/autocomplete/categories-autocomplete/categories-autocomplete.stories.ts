import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MdcMenuModule } from '@angular-mdc/web/menu';
import { MdcTextFieldModule } from '@angular-mdc/web/textfield';
import { MdcListModule } from '@angular-mdc/web/list';
import { MdcChipsModule } from '@angular-mdc/web/chips';
import { MdcIconModule, MdcIconRegistry } from '@angular-mdc/web/icon';

import { MdcAutocompleteComponent } from '../mdc-autocomplete/mdc-autocomplete.component';
import { HttpClientModule } from '@angular/common/http';
import { categories } from 'src/app/tournaments/components/tournaments.stories';
import { CategoriesAutocompleteComponent } from './categories-autocomplete.component';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';

export default {
  title: 'Components/Category',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [CategoriesAutocompleteComponent, MdcAutocompleteComponent],
      imports: [MdcMenuModule, MdcTextFieldModule, MdcListModule, MdcChipsModule, MdcIconModule, HttpClientModule],
      providers: [ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: CategoriesAutocompleteComponent,
  props: {
    items: object('items', categories)
  }
});

NewEmpty.story = {
  name: 'new empty'
};

export const Filled = () => ({
  component: CategoriesAutocompleteComponent,
  props: {
    items: object('items', categories),
    selected: object('selected', categories.slice(0, 0 + 3))
  }
});

Filled.story = {
  name: 'filled'
};
