import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureContendersComponent } from './pure-contenders.component';

describe('PureContendersComponent', () => {
  let component: PureContendersComponent;
  let fixture: ComponentFixture<PureContendersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureContendersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureContendersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
