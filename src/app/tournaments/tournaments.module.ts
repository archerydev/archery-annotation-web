import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TournamentsRoutingModule } from './tournaments-routing.module';
import { TournamentsComponent } from './containers/tournaments/tournaments.component';
import { EntityMetadataMap, EntityDefinitionService, EntityDataService, PLURAL_NAMES_TOKEN } from '@ngrx/data';
import { TournamentsResolver } from './services/tournaments.resolver';
import { TournamentEntityService } from './services/tournament-entity.service';
import { TournamentsDataService } from './services/tournaments-data.service';
import { ContenderEntityService } from './services/contender-entity.service';
import { ContendersDataService } from './services/contenders-data.service';
import { PureTournamentsCardListComponent } from './components/pure-tournaments-card-list/pure-tournaments-card-list.component';
import { MaterialModule } from '../material.module';
import { PureTournamentCardComponent } from './components/pure-tournament-card/pure-tournament-card.component';
import { PureTournamentsComponent } from './components/pure-tournaments/pure-tournaments.component';
import { PureTournamentComponent } from './components/pure-tournament/pure-tournament.component';
import { TournamentComponent } from './containers/tournament/tournament.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PureContenderEditComponent } from './components/pure-contender-edit/pure-contender-edit.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ReactiveFormsModule } from '@angular/forms';
import { CategoriesDataService } from './services/categories-data.service';
import { PureContenderItemComponent } from './components/pure-contender-item/pure-contender-item.component';
import { CategoryEntityService } from './services/category-entity.service';
import { ContenderEditComponent } from './containers/contender-edit/contender-edit.component';
import { PureContendersComponent } from './components/pure-contenders/pure-contenders.component';
import { PureLocationPickerComponent } from './components/pure-location-picker/pure-location-picker.component';
import { TournamentEditComponent } from './containers/tournament-edit/tournament-edit.component';
import { PureFileEditComponent } from './components/pure-file-edit/pure-file-edit.component';
import { QuillModule } from 'ngx-quill';
import { AutocompleteModule } from '../autocomplete/autocomplete.module';
import { HttpClientModule } from '@angular/common/http';
import { TournamentFormComponent } from './components/tournament-form/tournament-form.component';
import { AdminComponent } from './containers/admin/admin.component';
import { ClubsModule } from '../clubs/clubs.module';

const entityMetadada: EntityMetadataMap = {
  Tournament: {},
  Contender: {},
  Category: {},
  Mailer: {}
};
export const morePluralNames = {
  // Case matters. Match the case of the entity name.
  Category: 'Categories'
};

@NgModule({
  declarations: [
    TournamentsComponent,
    PureTournamentsCardListComponent,
    PureTournamentCardComponent,
    PureTournamentsComponent,
    PureTournamentComponent,
    TournamentComponent,
    PureContenderEditComponent,
    PureContenderItemComponent,
    ContenderEditComponent,
    PureContendersComponent,
    PureLocationPickerComponent,
    TournamentEditComponent,
    PureFileEditComponent,
    TournamentFormComponent,
    AdminComponent
  ],
  imports: [
    CommonModule,
    TournamentsRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    LeafletModule,
    ReactiveFormsModule,
    AutocompleteModule,
    HttpClientModule,
    ClubsModule,
    QuillModule.forRoot()
  ],
  providers: [
    TournamentEntityService,
    TournamentsResolver,
    TournamentsDataService,
    ContenderEntityService,
    ContendersDataService,
    CategoryEntityService,
    CategoriesDataService,
    { provide: PLURAL_NAMES_TOKEN, multi: true, useValue: morePluralNames }
  ],
  entryComponents: [
    ContenderEditComponent,
    PureFileEditComponent
  ]
})
export class TournamentsModule {
  constructor(
    eds: EntityDefinitionService,
    entityDataService: EntityDataService,
    tournamentsDataService: TournamentsDataService,
    contendersDataService: ContendersDataService,
    categoriesDataService: CategoriesDataService
  ) {
    eds.registerMetadataMap(entityMetadada);
    entityDataService.registerService('Tournament', tournamentsDataService);
    entityDataService.registerService('Contender', contendersDataService);
    entityDataService.registerService('Category', categoriesDataService);

  }
}
