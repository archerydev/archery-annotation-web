import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { PureTournamentCardComponent } from './pure-tournament-card.component';
import { MaterialModule } from 'src/app/material.module';

import { tournament, tourActions } from '../tournaments.stories';

const props = {
  viewTournament: tourActions.viewTournament,
  deleteTournament: tourActions.deleteTournament,
};

export default {
  title: 'Tournaments/TournamentCard',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureTournamentCardComponent],
      imports: [MaterialModule],
    }),
  ],
};

export const Default = () => ({
  component: PureTournamentCardComponent,
  props: {
    ...props,
    tournament: object('tournament', tournament),
  },
});

Default.story = {
  name: 'default',
};
