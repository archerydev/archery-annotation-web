import { DataSources } from './mailer-data';
import { IdModel } from 'kuzzle-ngrx';

export const FilterTemplateMapping = {
  dynamic: 'strict',
  _meta: {
    modelVersion: '1'
  },
  properties: {
    description: { type: 'text' },
    filter: { type: 'text' }
  }
};

export interface Subscription {
  collection: string;
  filters: string;
  notificationAction: string;
  notificationType: string;
}

export interface MailTemplate {
  from: string;
  to: string;
  subject: string;
  body: string;
}

// Mailer templates that will generate a mailer
// Not saved for the moment on the db
export interface MailerTemplate extends IdModel {
  description: string;
  mailTemplate: MailTemplate;
  subscription: Subscription;
}

export interface DataSource {
  description: string;
  collection: string;
  query: string;
  relationType: '1-1' | '1-n';
}

// Reference to be stored on the master objects that want to list their mailers
export interface MailerDesc extends IdModel {
  description: string;
}

//Subscriptions created that will send mails for the provided filter
export interface Mailer extends MailerDesc {
  masterCollection: string; //Name of the collection for which this mailer is associated
  masterId: string; //Master object id for the mailer
  mailTemplate: MailTemplate;
  subscription: Subscription;
  datasources: DataSource[];
}

export const emptyMailer: Mailer = {
  id: null,
  //The master is the object info (collection and id) for which this
  //mailer is active
  masterCollection: null,
  masterId: null,
  description: '',
  mailTemplate: {
    from: null,
    to: null,
    subject: null,
    body: null
  },
  subscription: {
    collection: 'contender',
    filters: '{}',
    notificationAction: 'create',
    notificationType: 'document'
  },
  datasources: DataSources
};

export const mailerMapping = {
  dynamic: 'strict',
  _meta: {
    modelVersion: '1'
  },
  properties: {
    masterCollection: { type: 'text' },
    masterId: { type: 'text' },
    description: { type: 'text' },
    mailTemplate: {
      properties: {
        body: {
          type: 'text'
        },
        from: {
          type: 'text'
        },
        subject: {
          type: 'text'
        },
        to: {
          type: 'text'
        }
      }
    },
    subscription: {
      properties: {
        collection: {
          type: 'text'
        },
        filters: {
          type: 'text'
        },
        notificationAction: {
          type: 'text'
        },
        notificationType: {
          type: 'text'
        }
      }
    },
    datasources: {
      properties: {
        description: {
          type: 'text'
        },
        collection: {
          type: 'text'
        },
        query: {
          type: 'text'
        },
        relationType: {
          type: 'text'
        }
      }
    }
  }
};

export const mailerModuleMappings = {
  Mailer: mailerMapping
};
