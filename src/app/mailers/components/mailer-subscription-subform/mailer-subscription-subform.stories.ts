import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { action } from '@storybook/addon-actions';
import { MailerSubscriptionSubFormComponent } from './mailer-subscription-subform.component';
import { Collections, Actions, Types } from '../../model/mailer-data';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
  collections: object('collections', Collections),
  actions: object('actions', Actions),
  types: object('types', Types),
  ngModelChange: action('ngModelChange')
};

export default {
  title: 'Mailers/SubForms/Subscription',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [MailerSubscriptionSubFormComponent],
      imports: [ReactiveFormsModule, MaterialModule, FlexLayoutModule],
      providers: [FormBuilder, ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: MailerSubscriptionSubFormComponent,
  props: {
    ngModel: null,
    ...props
  }
});

NewEmpty.story = {
  name: 'new empty'
};

// export const Edit = () => ({
//   component: GeolocationSubFormComponent,
//   props: {
//     ngModel: { null
//     },
//     ...props,
//   }
// });

// Edit.story = {
//   name: 'edit'
// };
