import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Club } from '../../model/club';

@Component({
  selector: 'app-pure-club-item, [appPureClubItem]',
  exportAs: 'appPureClubItem',
  templateUrl: './pure-club-item.component.html',
  styleUrls: ['./pure-club-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureClubItemComponent implements OnInit {

  @Input() club: Club;
  @Output() editClub = new  EventEmitter<Club>();

  constructor() { }

  ngOnInit() {
  }

}
