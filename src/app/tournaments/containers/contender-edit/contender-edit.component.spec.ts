import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenderEditComponent } from './contender-edit.component';

describe('ContenderEditComponent', () => {
  let component: ContenderEditComponent;
  let fixture: ComponentFixture<ContenderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
