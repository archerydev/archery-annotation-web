import { Position } from 'geojson';
import { IdModel } from 'kuzzle-ngrx';


export interface GeoLocation {
  description: string;
  geoPos?: Position;
  streetName?: string;
  streetNumber?: string;
  postalCode?: string;
  city?: string;
  region?: string;
}

export interface Club extends IdModel {
  name: string;
  imageUrl?: string;
  mail?: string;
  location?: GeoLocation;
}

export const emptyLocation: GeoLocation = {
  description: '',
  geoPos: null,
  streetName: null,
  streetNumber: null,
  postalCode: null,
  city: null,
  region: null
};

export const emptyClub: Club = {
  id: '',
  name: '',
  mail: '',
  imageUrl: '',
  location: null
};

export const clubMapping = {
  dynamic: 'strict',
  _meta: {
    modelVersion: '1'
  },
  properties: {
    name: { type: 'text' },
    imageUrl: { type: 'text' },
    mail: { type: 'keyword' },
    location: {
      dynamic: 'strict',
      properties: {
        description: { type: 'text' },
        geoPos: { type: 'geo_point' },
        streetName: { type: 'text' },
        streetNumber: { type: 'text' },
        postalCode: { type: 'text' },
        city: { type: 'text' },
        region: { type: 'text' }
      }
    }
  }
};

export const clubModuleMappings = {
  Club: clubMapping
};
