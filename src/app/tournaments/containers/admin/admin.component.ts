import { Component, OnInit } from '@angular/core';
import { TournamentEntityService } from '../../services/tournament-entity.service';
import { CategoryEntityService } from '../../services/category-entity.service';
import { ClubEntityService } from '../../../clubs/services/club-entity.service';
import { KuzzleSchemaUpdaterService } from 'kuzzle-ngrx';
import { setupClubs } from '../../../clubs/model/club-data';
import { setupCategories } from '../../model/category-data';
import { setupTournaments } from '../../model/tournament-data';
import { clubModuleMappings } from '../../../clubs/model/club';
import { mailerModuleMappings } from '../../../mailers/model/mailer';
import { categoryModuleMappings } from '../../model/category';
import { tournamentModuleMappings } from '../../model/tournament';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  constructor(
    private tournamentsService: TournamentEntityService,
    private categoriesService: CategoryEntityService,
    private clubsService: ClubEntityService,
    private updaterService: KuzzleSchemaUpdaterService
  ) {
    console.log("admin");
  }

  ngOnInit() {}

  onUpdateDbData() {
    setupClubs().forEach(t => this.clubsService.add(t));
    setupCategories().forEach(t => this.categoriesService.add(t));
    setupTournaments().forEach(t => this.tournamentsService.add(t));
  }

  onUpdateDbSchema() {
    //this.updaterService.updateSchemas([mailerModuleMappings]);
    this.updaterService.updateSchemas([
      clubModuleMappings,
      mailerModuleMappings,
      categoryModuleMappings,
      tournamentModuleMappings
    ]);
  }

  onUploadFile() {}

  onAddClub() {
    // const dialogConfig = new MdcDialogConfig();

    // dialogConfig.autoFocus = true;
    // dialogConfig.data = {
    //   mode: 'create'
    // };

    // this.dialog.open(ClubEditComponent, dialogConfig);
  }
}
