import { Category } from './category';
import { Club, GeoLocation } from '../../clubs/model/club';
import { MailerDesc } from '../../mailers/model/mailer';
import { IdModel } from 'kuzzle-ngrx';

export interface FileDesc {
  name: string;
  url: string;
}

export interface Tournament extends IdModel {
  description: string;
  longDescription: string;
  iconUrl?: string;
  imageUrl?: string;
  price: number;
  date: Date;
  hostingClub?: Club;
  location?: GeoLocation;
  pdfUrl?: string;
  categories: Category[];
  mailing: MailerDesc[]; //ids of mail subscriptions created for the tournamnent.
  files: FileDesc[];
}

export const emptyTournament: Tournament = {
  id: null,
  description: null,
  longDescription: null,
  iconUrl: null,
  imageUrl: null,
  price: 0,
  date: new Date(),
  hostingClub: null,
  location: null,
  categories: [],
  mailing: [],
  files: []
};

export const tournamentMapping = {
  dynamic: 'strict',
  _meta: {
    modelVersion: '1'
  },
  properties: {
    description: { type: 'text' },
    longDescription: { type: 'text' },
    iconUrl: { type: 'text' },
    imageUrl: { type: 'text' },
    date: { type: 'date' },
    price: { type: 'short' },
    hostingClub: {
      dynamic: 'strict',
      properties: {
        id: { type: 'text' },
        name: { type: 'text' },
        imageUrl: { type: 'text' }
      }
    },
    location: {
      dynamic: 'strict',
      properties: {
        description: { type: 'text' },
        geoPos: { type: 'geo_point' },
        streetName: { type: 'text' },
        streetNumber: { type: 'text' },
        postalCode: { type: 'text' },
        city: { type: 'text' },
        region: { type: 'text' }
      }
    },
    pdfUrl: { type: 'text' },
    categories: {
      dynamic: 'strict',
      properties: {
        id: { type: 'text' },
        division: { type: 'text' },
        archeryClass: { type: 'text' }
      }
    },
    mailing: {
      dynamic: 'strict',
      properties: {
        id: { type: 'text' },
        description: { type: 'text' }
      }
    },
    files: {
      dynamic: 'strict',
      properties: {
        name: { type: 'text' },
        url: { type: 'text' }
      }
    }
  }
};

export interface Contender extends IdModel {
  tournamentId: string;
  name: string;
  archerLicense: string;
  email: string;
  club: Club;
  category: Category;
}

export const emptyContender: Contender = {
  id: null,
  tournamentId: null,
  name: '',
  archerLicense: '',
  email: '',
  club: null,
  category: null
};

export const contenderMapping = {
  dynamic: 'strict',
  _meta: {
    modelVersion: '1'
  },
  properties: {
    tournamentId: { type: 'text' },
    name: { type: 'text' },
    archerLicense: { type: 'text' },
    email: { type: 'keyword' },
    club: {
      dynamic: 'strict',
      properties: {
        id: { type: 'text' },
        name: { type: 'text' },
        imageUrl: { type: 'text' }
      }
    },
    category: {
      dynamic: 'strict',
      properties: {
        id: { type: 'text' },
        division: { type: 'text' },
        archeryClass: { type: 'text' }
      }
    }
  }
};

export const tournamentModuleMappings = {
  Tournament: tournamentMapping,
  Contender: contenderMapping
};
