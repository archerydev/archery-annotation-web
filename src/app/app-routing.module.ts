import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MailEditorComponent } from './shared/mail-editor/mail-editor.component';

const routes: Routes = [
  {
    path: 'tournaments',
    loadChildren: () => import('./tournaments/tournaments.module').then(m => m.TournamentsModule)
  },
  {
    path: 'clubs',
    loadChildren: () => import('./clubs/clubs.module').then(m => m.ClubsModule)
  },
  {
    path: 'mailers',
    loadChildren: () => import('./mailers/mailers.module').then(m => m.MailersModule)
  },
  {
    path: 'mailEditor',
    component: MailEditorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
