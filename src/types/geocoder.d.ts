import * as L from 'leaflet';
declare module 'leaflet' {
  namespace control {
    function geocoder(options?: any): Control.GeoCoder;
  }
  namespace Control {
    interface GeoCoder {
      addTo(map: L.Map): any;
    }
  }
}
