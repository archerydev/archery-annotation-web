import { action } from '@storybook/addon-actions';
import { MdcDialogConfig } from '@angular-mdc/web/dialog';

export const MdcDialogRefFactory = depHolder => {
  const dialogConfig = new MdcDialogConfig();
  dialogConfig.data = { ...depHolder.data };

  return {
    _portalInstance: {
      _config: dialogConfig
    },
    close: (dialogResult: any) => {},
    opened: () => {}
  };
};
