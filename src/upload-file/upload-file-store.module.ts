import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { featureReducer } from './store/reducer';
import { UploadFileEffects } from './store/effects';
import { UploadFileDialogComponent } from './upload-file-dialog.component';
import { FileListDialogComponent } from './file-list-dialog.component';
import { MaterialModule } from 'src/app/material.module';
import { MdcDialogModule } from '@angular-mdc/web/dialog';
import { KuzzleNgrxModule } from 'kuzzle-ngrx';


//https://blog.ag-grid.com/managing-file-uploads-with-ngrx-2/


@NgModule({
  declarations: [UploadFileDialogComponent, FileListDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    MdcDialogModule,
    StoreModule.forFeature('uploadFile', featureReducer),
    EffectsModule.forFeature([UploadFileEffects]),
    KuzzleNgrxModule
  ],
  entryComponents: [UploadFileDialogComponent, FileListDialogComponent]
})
export class UploadFileStoreModule { }
