import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Club } from '../../model/club';

@Component({
  selector: 'app-pure-clubs',
  templateUrl: './pure-clubs.component.html',
  styleUrls: ['./pure-clubs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureClubsComponent implements OnInit {

  @Input() clubs: Club[];
  @Output() editClub = new  EventEmitter<Club>();

  @Output() newClub = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

}
