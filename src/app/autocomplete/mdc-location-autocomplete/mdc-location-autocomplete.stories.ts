import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { action } from '@storybook/addon-actions';
import { MdcLocationAutocompleteComponent } from './mdc-location-autocomplete.component';
import { MdcAutocompleteComponent } from '../mdc-autocomplete/mdc-autocomplete.component';
import { HttpClientModule } from '@angular/common/http';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
   //saveClub: clubActions.saveClub
  // deleteClub: clubActions.deleteClub,
  // selectImage: clubActions.selectImage,
  // closeDialog: clubActions.closeDialog,
};

// let GeoLocationSubFormFactory = () => {
//   const component =  new GeolocationSubFormComponent();
//   component.writeValue({description: "test", geoPos: null});
//   console.log("here");
//   return component;
// };

export default {
  title: 'Components/Location',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [MdcLocationAutocompleteComponent, MdcAutocompleteComponent],
      imports: [
        MaterialModule,
        HttpClientModule],
      providers: [ErrorStateMatcher, MdcIconRegistry],
    }),
  ],
};

export const NewEmpty = () => ({
  component: MdcLocationAutocompleteComponent,
 });

NewEmpty.story = {
  name: 'new empty',
};

