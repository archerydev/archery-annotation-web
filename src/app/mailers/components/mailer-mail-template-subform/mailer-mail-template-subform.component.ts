import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { subformComponentProviders, NgxSubFormComponent, Controls } from 'ngx-sub-form';
import { MailTemplate } from '../../model/mailer';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-mailer-mail-template-subform',
  templateUrl: './mailer-mail-template-subform.component.html',
  styleUrls: ['./mailer-mail-template-subform.component.scss'],
  providers: subformComponentProviders(MailerMailTemplateSubFormComponent),
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MailerMailTemplateSubFormComponent extends NgxSubFormComponent<MailTemplate> {

  protected getFormControls(): Controls<MailTemplate> {
    return {
      from: new FormControl(null, Validators.required),
      to: new FormControl(null, Validators.required),
      subject: new FormControl(null, Validators.required),
      body: new FormControl(null, Validators.required)
  };
  }


}
