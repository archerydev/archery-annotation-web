import { Component, OnInit, Input, NgZone, ChangeDetectorRef, Inject } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { FileDesc } from '../../model/tournament';
import { FileListDialogComponent } from 'src/upload-file/file-list-dialog.component';
import { MdcDialog, MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';

@Component({
  selector: 'app-pure-file-edit',
  templateUrl: './pure-file-edit.component.html',
  styleUrls: ['./pure-file-edit.component.scss']
})
export class PureFileEditComponent implements OnInit {


  file: FileDesc;
  form = this.fb.group({
    name: [null, Validators.required]
    // url: [null, Validators.required]
  });

  constructor(
    private dialog: MdcDialog,
    private fb: FormBuilder,
    public dialogRef: MdcDialogRef<PureFileEditComponent>,
    @Inject(MDC_DIALOG_DATA) data
  ) {
    this.file = data.file;
    this.form.patchValue({...this.file});

  }

  onSelectFile() {
    const dialogRef = this.dialog.open(FileListDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null && result !== '') {
        const filePath = (result as { file_path: string }).file_path;
        this.file = {
          ...this.file,
          url: filePath
        };
      }
    });
  }

  ngOnInit() {
  }

  onSave() {
    this.dialogRef.close(this.file);
  }

  onClose() {
    this.dialogRef.close();
  }

}
