import {
  Component,
  OnInit,
  AfterViewInit,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { FormControlSuperclass, provideValueAccessor } from 's-ng-utils';
import { Category } from 'src/app/tournaments/model/category';
import { Subject } from 'rxjs';
import { MdcAutocompleteComponent } from '../mdc-autocomplete/mdc-autocomplete.component';
import { MdcMenu } from '@angular-mdc/web/menu';

@Component({
  selector: 'categories-autocomplete',
  templateUrl: './categories-autocomplete.component.html',
  styleUrls: ['./categories-autocomplete.component.scss'],
  providers: [provideValueAccessor(CategoriesAutocompleteComponent)],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesAutocompleteComponent extends FormControlSuperclass<Category[]>
  implements AfterViewInit, OnChanges {
  //TODO: manage when there are not categories
  @ViewChild(MdcAutocompleteComponent, { static: false }) _autocomplete!: MdcAutocompleteComponent<Category>;
  @Input() selected: Category[] = new Array<Category>();
  @Input() items: Category[] = new Array<Category>();

  filteredCategories: Subject<Category[]> = new Subject<Category[]>();

  filteredFn = (category: Category) => `${category.division} ${category.archeryClass}`;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.filterSelectedCategories();
  }

  onSelectItem(value: Category) {
    this.selected = this.selected.concat(value);
    this.emitOutgoingValue(this.selected);
    this._autocomplete.writeValue(null);
    const fItems = this.items.filter(i => !this.selected.includes(i));
    this.filteredCategories.next(fItems);
  }

  onEnterPress(value: string) {
    const fItems = this.getFilteredItems(value);
    if (fItems.length === 1) {
      this.onSelectItem(fItems[0]);
      this._autocomplete.hideOptions();
    }
  }

  removeCategory(category: Category) {
    this.selected = this.selected.filter(item => item !== category);
    this.emitOutgoingValue(this.selected);
    this.filterSelectedCategories();
    this._autocomplete.hideOptions();
  }

  ngAfterViewInit() {
    this.filterSelectedCategories();
  }

  handleIncomingValue(value: Category[]): void {
    this.selected = value;
    this.filterSelectedCategories();
  }

  onFieldChange(value) {
    this.filteredCategories.next(this.getFilteredItems(value));
  }

  getFilteredItems(value: string): Category[] {
    let fItems = this.items.filter(i => !this.selected.map(c => c.id).includes(i.id));
    if (value && value !== '') {
      fItems = fItems.filter(item =>
        this.filteredFn(item)
          .toUpperCase()
          .includes(value.toUpperCase() ?? '')
      );
    }
    return fItems;
  }

  filterSelectedCategories() {
    const fItems = this.items.filter(i => !this.selected.map(c => c.id).includes(i.id));
    this.filteredCategories.next(fItems);
  }

  onFocus(value: boolean) {
    // console.log(value);
    // if (value)    {
    //   // const fItems = this.items.filter(i => !this.selected.includes(i));
    //   // this.filteredCategories.next(fItems);
    // }
  }
  clearQuery() {
    this._autocomplete.writeValue(null);
  }
}
