import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PureTournamentsCardListComponent } from './pure-tournaments-card-list.component';
import { Tournament } from '../../model/tournament';
import { setupTournaments } from '../../model/tournament-data';
import { TournamentItemComponent } from '../pure-tournament-card/pure-tournament-card.component';

export const defaultTournaments: Tournament[] = setupTournaments();

describe('TournamentsCardListComponent', () => {
  let component: PureTournamentsCardListComponent;
  let fixture: ComponentFixture<PureTournamentsCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureTournamentsCardListComponent, TournamentItemComponent ],
    })
    .overrideComponent(TournamentItemComponent, {set: {
      template: '<div></div>'
    }})
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureTournamentsCardListComponent);
    component = fixture.componentInstance;
    component.tournaments = defaultTournaments;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
