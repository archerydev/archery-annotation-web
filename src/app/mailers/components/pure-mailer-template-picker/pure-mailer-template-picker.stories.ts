import { moduleMetadata } from '@storybook/angular';
import { withKnobs } from '@storybook/addon-knobs';
import { PureMailerTemplatePickerComponent } from './pure-mailer-template-picker.component';
import { defaultMailerTemplates, MdcDialogRefFactory } from '../../../tournaments/components/tournaments.stories';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';
import { MaterialModule } from 'src/app/material.module';

const props = {};

export default {
  title: 'Mailers/Template Picker',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureMailerTemplatePickerComponent],
      imports: [MaterialModule],
      providers: [
        {
          provide: MdcDialogRef,
          useFactory: MdcDialogRefFactory,
          deps: [MDC_DIALOG_DATA],
        },
        {
          provide: MDC_DIALOG_DATA,
          useValue: {
            templates: defaultMailerTemplates,
          },
        },
      ],
    }),
  ],
};

export const Default = () => ({
  component: PureMailerTemplatePickerComponent,
  props: {
    ...props,
  },
});

Default.story = {
  name: 'default',
};
