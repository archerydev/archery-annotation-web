import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureTournamentComponent } from './pure-tournament.component';
import { Tournament } from '../../model/tournament';
import { setupTournaments } from '../../model/tournament-data';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/material.module';

export const defaultTournaments: Tournament[] = setupTournaments();
export const tournament = defaultTournaments[0];
describe('PureTournamentComponent', () => {
  let component: PureTournamentComponent;
  let fixture: ComponentFixture<PureTournamentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureTournamentComponent ],
      imports: [MaterialModule, FlexLayoutModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureTournamentComponent);
    component = fixture.componentInstance;
    component.tournament = tournament;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
