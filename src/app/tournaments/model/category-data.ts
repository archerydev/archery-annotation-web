import { Category } from './category';

export const CATEGORIES: any = {
  1: {
    id: 'RW',
    division: 'Recurve',
    archeryClass: 'Women'
  },
  2: {
    id: 'RM',
    division: 'Recurve',
    archeryClass: 'Men'
  },
  3: {
    id: 'RCW',
    division: 'Recurve',
    archeryClass: 'Cadet Women'
  },
  4: {
    id: 'RCM',
    division: 'Recurve',
    archeryClass: 'Cadet Men'
  },
  5: {
    id: 'RJW',
    division: 'Recurve',
    archeryClass: 'Junior Women'
  },
  6: {
    id: 'RJM',
    division: 'Recurve',
    archeryClass: 'Junior Men'
  },
  7: {
    id: 'RMW',
    division: 'Recurve',
    archeryClass: 'Master Women'
  },
  8: {
    id: 'RMM',
    division: 'Recurve',
    archeryClass: 'Master Men'
  },
  9: {
    id: 'RX',
    division: 'Recurve',
    archeryClass: 'Mix'
  },
  10: {
    id: 'RN',
    division: 'Recurve',
    archeryClass: 'Novel'
  },
  11: {
    id: 'RIX',
    division: 'Recurve',
    archeryClass: 'Child Mix'
  },
  12: {
    id: 'CW',
    division: 'Compound',
    archeryClass: 'Women'
  },
  13: {
    id: 'CM',
    division: 'Compound',
    archeryClass: 'Men'
  },
  14: {
    id: 'CCW',
    division: 'Compound',
    archeryClass: 'Cadet Women'
  },
  15: {
    id: 'CCM',
    division: 'Compound',
    archeryClass: 'Cadet Men'
  },
  16: {
    id: 'CJW',
    division: 'Compound',
    archeryClass: 'Junior Women'
  },
  17: {
    id: 'CJM',
    division: 'Compound',
    archeryClass: 'Junior Men'
  },
  18: {
    id: 'CMW',
    division: 'Compound',
    archeryClass: 'Master Women'
  },
  19: {
    id: 'CMM',
    division: 'Compound',
    archeryClass: 'Master Men'
  },
  20: {
    id: 'CX',
    division: 'Compound',
    archeryClass: 'Mix'
  },
  21: {
    id: 'CN',
    division: 'Compound',
    archeryClass: 'Novel'
  },
  22: {
    id: 'CIX',
    division: 'Compound',
    archeryClass: 'Child Mix'
  },
  23: {
    id: 'IW',
    division: 'Instintive',
    archeryClass: 'Women'
  },
  24: {
    id: 'IM',
    division: 'Instintive',
    archeryClass: 'Men'
  },
  25: {
    id: 'LW',
    division: 'Longbow',
    archeryClass: 'Women'
  },
  26: {
    id: 'LM',
    division: 'Longbow',
    archeryClass: 'Men'
  },
  27: {
    id: 'IIX',
    division: 'Instintive',
    archeryClass: 'Child Mix'
  },
  28: {
    id: 'ILX',
    division: 'Longbow',
    archeryClass: 'Child Mix'
  },
  29: {
    id: 'IN',
    division: 'Instintive',
    archeryClass: 'Novel'
  },
  30: {
    id: 'LN',
    division: 'Longbow',
    archeryClass: 'Novel'
  }
};

export function setupCategories(): Category[] {
  return Object.values(CATEGORIES)
    .map(t => ({
      ...(t as object)
    })) as Category[];
}
