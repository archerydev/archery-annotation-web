import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailerEditComponent } from './mailer-edit.component';

describe('MailerEditComponent', () => {
  let component: MailerEditComponent;
  let fixture: ComponentFixture<MailerEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailerEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
