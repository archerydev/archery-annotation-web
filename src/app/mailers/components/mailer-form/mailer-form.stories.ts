import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { action } from '@storybook/addon-actions';
import { Collections, Actions, Types } from '../../model/mailer-data';
import { MailerFormComponent } from './mailer-form.component';
import { MailerSubscriptionSubFormComponent } from '../mailer-subscription-subform/mailer-subscription-subform.component';
import { MailerMailTemplateSubFormComponent } from '../mailer-mail-template-subform/mailer-mail-template-subform.component';
import { PureMailerTemplatePickerComponent } from '../pure-mailer-template-picker/pure-mailer-template-picker.component';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
  collections: object('collections', Collections),
  actions: object('actions', Actions),
  types: object('types', Types),
  ngModelChange: action('ngModelChange')
};

export default {
  title: 'Mailers/Form',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [
        MailerFormComponent,
        MailerMailTemplateSubFormComponent,
        MailerSubscriptionSubFormComponent,
        PureMailerTemplatePickerComponent
      ],
      imports: [ReactiveFormsModule, MaterialModule, FlexLayoutModule],
      entryComponents: [PureMailerTemplatePickerComponent],
      providers: [PureMailerTemplatePickerComponent, FormBuilder, ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: MailerFormComponent,
  props: {
    ngModel: null,
    ...props
  }
});

NewEmpty.story = {
  name: 'new empty'
};
