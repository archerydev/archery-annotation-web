import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { club, clubActions } from '../../../tournaments/components/tournaments.stories';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { emptyClub, emptyLocation } from '../../model/club';
import { action } from '@storybook/addon-actions';
import { GeolocationSubFormComponent } from './geolocation-sub-form.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { HttpClientModule } from '@angular/common/http';
import { AutocompleteModule } from 'src/app/autocomplete/autocomplete.module';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

const props = {
    country: object('country', 'ES'),
    ngModelChange: action('ngModelChange'),
  //saveClub: clubActions.saveClub
  // deleteClub: clubActions.deleteClub,
  // selectImage: clubActions.selectImage,
  // closeDialog: clubActions.closeDialog,
};

// let GeoLocationSubFormFactory = () => {
//   const component =  new GeolocationSubFormComponent();
//   component.writeValue({description: "test", geoPos: null});
//   console.log("here");
//   return component;
// };

export default {
  title: 'Clubs/SubForms/Geolocation',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [GeolocationSubFormComponent],
      imports: [
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        LeafletModule,
        HttpClientModule,
        AutocompleteModule
      ],
      providers: [FormBuilder, ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: GeolocationSubFormComponent,
  props: {
    ngModel: {...emptyLocation},
    ...props,
    mode: 'create'
  }
});

NewEmpty.story = {
  name: 'new empty'
};

export const Edit = () => ({
  component: GeolocationSubFormComponent,
  props: {
    ngModel: {
      city: 'Rivas-Vaciamadrid',
      description: 'Polideportivo Municipal Cerro del Telégrafo, Partija,La, España',
      geoPos: [-3.544807, 40.357562],
      region: 'Madrid',
      streetNumber: null,
      streetName: null,
      postalCode: null
    },
    ...props,
  }
});

Edit.story = {
  name: 'edit'
};
