import { Club } from './club';

export const CLUBS: any = {
  0: {
    id: '0',
    name: 'Sin Club Asignado',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_sinclub.png'
  },
  1: {
    id: '1',
    name: 'A.D. Collado Mediano',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_colladomediano.png',
    mail: '',
    location: {
      description: 'Mi lugar',
      geoPos: null
    }
  },
  2: {
    id: '2',
    name: 'Arco Boadilla',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arcoboadilla.png'
  },
  3: {
    id: '3',
    name: 'Arco Club Navalmoral',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_navalmoral.png'
  },
  4: {
    id: '4',
    name: 'Arcomadrid',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arcomadrid.png'
  },
  5: {
    id: '5',
    name: 'Arcosoto',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arcosoto.png'
  },
  6: {
    id: '6',
    name: 'Arcus',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arcus.png'
  },
  7: {
    id: '7',
    name: 'Arqueros 10 Galapagar',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arqueros10.png'
  },
  8: {
    id: '8',
    name: 'Arqueros de Aranjuez',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arcoaranjuez.png'
  },
  9: {
    id: '9',
    name: 'Arqueros de Colmenar',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_colmenar.png'
  },
  10: {
    id: '10',
    name: 'Arqueros de Getafe',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosgetafe.png'
  },
  11: {
    id: '11',
    name: 'Arqueros de Leganés',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdeleganes.png'
  },
  12: {
    id: '12',
    name: 'Arqueros de Madrid',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdemadrid.png'
  },
  13: {
    id: '13',
    name: 'Arqueros de Manzanares',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdemanzanares.png'
  },
  14: {
    id: '14',
    name: 'Arqueros de Marqués de Suances',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquersomarquesdesuances.png'
  },
  15: {
    id: '15',
    name: 'Arqueros de Pozuelo',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdepozuelo.png'
  },
  16: {
    id: '16',
    name: 'Arqueros de San Lorenzo',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdesanlorenzo.png'
  },
  17: {
    id: '17',
    name: 'Arqueros de Sol XIV',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdesolxiv.png'
  },
  18: {
    id: '18',
    name: 'Arqueros de Ulises',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdeulises.png'
  },
  19: {
    id: '19',
    name: 'Arqueros de Villa de Valdemorillo',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arquerosdevaldemorillo.png'
  },
  20: {
    id: '20',
    name: 'Caracal',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_caracal.png'
  },
  21: {
    id: '21',
    name: 'CD Arco Cuenca',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arcocuenca.png'
  },
  22: {
    id: '22',
    name: 'CDE Arco Las Rozas',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_sinclub.png'
  },
  23: {
    id: '23',
    name: 'CDETA Rivas Vaciamadrid',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_cdeta.png',
    location: {
      description: 'Polideportivo Cerro del Espino',
      geoPos: [-3.54166667, 40.35777778 ]
    },
  },
  24: {
    id: '24',
    name: 'CDETA Tarancón',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_etatarancon.png'
  },
  25: {
    id: '25',
    name: 'Club Artemisa',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_artemisa.png'
  },
  26: {
    id: '26',
    name: 'Club Deportivo Elemental Diez',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_arqueros10.png'
  },
  27: {
    id: '27',
    name: 'Club Águila Imperial',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_aguilaimperial.png'
  },
  28: {
    id: '28',
    name: 'Majaltobar',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_majaltobar.png'
  },
  29: {
    id: '29',
    name: 'Moratalaz',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_moratalaz.png'
  },
  30: {
    id: '30',
    name: 'Olimpo',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_olimpo.png'
  },
  31: {
    id: '31',
    name: 'Sagitta',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_sagitta.png'
  },
  32: {
    id: '32',
    name: 'Villa de Alcobendas',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_villadealcobendas.png'
  },
  33: {
    id: '33',
    name: 'Villa de Valdemoro',
    imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_valdemoro.png',
    mail: null,
    location: null
  }
};

export function setupClubs(): Club[] {
  return Object.values(CLUBS).map(t => ({
    ...(t as object)
  })) as Club[];
}
