import { Tournament, Contender, FileDesc } from './tournament';
import { sortTournamentsByDate } from './sort-tournaments-by-date';

export const emptyFile: FileDesc = {
  name: '',
  url: ''
};

export const TOURNAMENTS: any = {
  1: {
    id: '1',
    description: 'Espiral de Rivas 2020',
    longDescription:
      'El Club Deportivo Elemental de Tiro con Arco Rivas Vaciamadrid \
                      tiene el honor de invitaros a participar en el Trofeo la Espiral.',
    date: '2020-09-22T09:00:00',
    iconUrl:
      'https://www.arquerosderivas.org/images/documentos/club_competiciones/Torneo_La_Espiral/Cartel%20Espiral%202019.jpg',
    hostingClub: {
      id: '2',
      name: 'CDETA Rivas Vaciamadrid',
      imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_cdeta.png'
    },
    location: {
      city: 'Rivas-Vaciamadrid',
      description: 'Polideportivo Municipal Cerro del Telégrafo, Partija,La, España',
      geoPos: [-3.544807, 40.357562],
      region: 'Madrid'
    },
    price: 15,
    categories: [
      {
        id: 'IW',
        division: 'Instintive',
        archeryClass: 'Women'
      },
      {
        id: 'IM',
        division: 'Instintive',
        archeryClass: 'Men'
      },
      {
        id: 'LW',
        division: 'Longbow',
        archeryClass: 'Women'
      },
      {
        id: 'LM',
        division: 'Longbow',
        archeryClass: 'Men'
      }
    ],
    files: [
      {
        name: 'test1',
        url: 'test1'
      },
      {
        name: 'test2',
        url: 'test2'
      }
    ]
  },
  2: {
    id: '2',
    description: 'XIII Ciudad de Rivas',
    longDescription:
      'El Club Deportivo Elemental de Tiro con Arco Rivas Vaciamadrid \
                      tiene el honor de invitaros a participar en el XIII Trofeo "Ciudad de Rivas".',
    date: '2020-09-21T09:00:00',
    iconUrl:
      'https://www.arquerosderivas.org/images/documentos/club_competiciones/Torneo_La_Espiral/Cartel%20XII%20Ciudad%20de%20Rivas%202019.jpg',
    hostingClub: {
      id: '2',
      name: 'CDETA Rivas Vaciamadrid',
      imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_cdeta.png'
    },
    location: {
      city: 'Rivas-Vaciamadrid',
      description: 'Polideportivo Municipal Cerro del Telégrafo, Partija,La, España',
      geoPos: [-3.544807, 40.357562],
      region: 'Madrid'
    },
    price: 15,
    categories: [
      {
        id: 'RW',
        division: 'Recurve',
        archeryClass: 'Women'
      },
      {
        id: 'RM',
        division: 'Recurve',
        archeryClass: 'Men'
      },
      {
        id: 'RN',
        division: 'Recurve',
        archeryClass: 'Novel'
      },
      {
        id: 'CW',
        division: 'Compound',
        archeryClass: 'Women'
      },
      {
        id: 'CM',
        division: 'Compound',
        archeryClass: 'Men'
      }
    ],
    mailing: [
      {
        id: 1,
        description: 'Send mail on contender subscription'
      },
      {
        id: 2,
        description: 'Send mail on contender asignation'
      }
    ],
    files: [
      {
        name: 'test1',
        url: 'test1'
      },
      {
        name: 'test2',
        url: 'test2'
      }
    ]
  },
  3: {
    id: '3',
    description: 'XIII Ciudad de Rivas',
    longDescription:
      'El Club Deportivo Elemental de Tiro con Arco Rivas Vaciamadrid \
                      tiene el honor de invitaros a participar en el XIII Trofeo "Ciudad de Rivas".',
    date: '2020-09-21T09:00:00',
    iconUrl:
      'https://www.arquerosderivas.org/images/documentos/club_competiciones/Torneo_La_Espiral/Cartel%20XII%20Ciudad%20de%20Rivas%202019.jpg',
    price: 12,
    categories: [
      {
        id: 'RW',
        division: 'Recurve',
        archeryClass: 'Women'
      },
      {
        id: 'RM',
        division: 'Recurve',
        archeryClass: 'Men'
      },
      {
        id: 'RN',
        division: 'Recurve',
        archeryClass: 'Novel'
      },
      {
        id: 'CW',
        division: 'Compound',
        archeryClass: 'Women'
      },
      {
        id: 'CM',
        division: 'Compound',
        archeryClass: 'Men'
      }
    ],
    mailing: [
      {
        id: 1,
        description: 'Send mail on contender subscription'
      },
      {
        id: 2,
        description: 'Send mail on contender asignation'
      }
    ],
    files: [
      {
        name: 'test1',
        url: 'test1'
      },
      {
        name: 'test2',
        url: 'test2'
      }
    ]
  }
};

export function setupTournaments(): Tournament[] {
  return Object.values(TOURNAMENTS)
    .map(t => ({
      ...(t as object),
      date: new Date(t['date'])
    }))
    .sort(sortTournamentsByDate) as Tournament[];
}

export const CONTENDERS: any = {
  1: {
    tournamentId: '2',
    name: 'Obi Wan Kenobi',
    archerLicense: 'MAD12345',
    email: 'owk@deathstar.es',
    club: {
      id: '2',
      name: 'CDETA Rivas Vaciamadrid',
      imageUrl: 'https://minio.gpulido.com/kuzzle/archeryannotation/clubs/club_cdeta.png'
    },
    category: {
      id: 'RM',
      division: 'Recurve',
      archeryClass: 'Men'
    }
  }
};
export function setupContenders(): Contender[] {
  return Object.values(CONTENDERS).map(t => ({
    ...(t as object)
  })) as Contender[];
}
