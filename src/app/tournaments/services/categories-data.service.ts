import { Injectable } from '@angular/core';
import { Category } from '../model/category';
import { KuzzleDataService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';


@Injectable()
export class CategoriesDataService extends KuzzleDataService<Category> {
  constructor(kuzzle: KuzzleService) {
    super('Category', kuzzle);
  }
}
