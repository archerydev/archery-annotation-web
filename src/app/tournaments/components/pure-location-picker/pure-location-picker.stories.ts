import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { MaterialModule } from 'src/app/material.module';
import { PureLocationPickerComponent } from './pure-location-picker.component';

const props = {};

export default {
  title: 'Location/Picker',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureLocationPickerComponent],
      imports: [MaterialModule],
    }),
  ],
};

export const Default = () => ({
  component: PureLocationPickerComponent,
  props: {
    ...props,
  },
});

Default.story = {
  name: 'default',
};
