import { moduleMetadata } from '@storybook/angular';
import { withKnobs, object } from '@storybook/addon-knobs';

import {
  ErrorStateMatcher,
} from '@angular-mdc/web/form-field';
import {
  MdcMenuModule,
} from '@angular-mdc/web/menu';
import {
  MdcTextFieldModule,
} from '@angular-mdc/web/textfield';
import {
  MdcListModule,
} from '@angular-mdc/web/list';
import {
  MdcChipsModule,
} from '@angular-mdc/web/chips';
import {
  MdcIconModule,
  MdcIconRegistry
} from '@angular-mdc/web/icon';

import { MdcAutocompleteComponent } from '../mdc-autocomplete/mdc-autocomplete.component';
import { HttpClientModule } from '@angular/common/http';
import { MdcSearchLocationAutocompleteComponent } from './mdc-search-location-autocomplete.component';
import { MdcLocationAutocompleteComponent } from '../mdc-location-autocomplete/mdc-location-autocomplete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

export default {
  title: 'Components/SearchLocation',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [
        MdcSearchLocationAutocompleteComponent,
        MdcLocationAutocompleteComponent,
        MdcAutocompleteComponent
      ],
      imports: [
        MdcMenuModule,
        FlexLayoutModule,
        MdcTextFieldModule,
        MdcListModule,
        MdcChipsModule,
        MdcIconModule,
        HttpClientModule,
        ReactiveFormsModule,
        LeafletModule
      ],
      providers: [ErrorStateMatcher, MdcIconRegistry]
    })
  ]
};

export const NewEmpty = () => ({
  component: MdcSearchLocationAutocompleteComponent,
  props: {
    country: object('country', 'ES')
  }
});

NewEmpty.story = {
  name: 'new empty'
};

export const Edit = () => ({
  component: MdcSearchLocationAutocompleteComponent,
  props: {
    country: object('country', 'ES')
   // geoLocation: object('geoLocation', )

  }
});

Edit.story = {
  name: 'edit'
};
