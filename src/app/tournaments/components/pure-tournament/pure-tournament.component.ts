import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Tournament } from '../../model/tournament';
import { tileLayer, marker, icon, Marker, MapOptions, Map } from 'leaflet';
import * as L from 'leaflet';
import 'pelias-leaflet-plugin';
import { Observable, of, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-pure-tournament',
  templateUrl: './pure-tournament.component.html',
  styleUrls: ['./pure-tournament.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureTournamentComponent implements OnInit {
  @Input() tournament: Tournament;
  @Output() register: EventEmitter<any> = new EventEmitter();

  marker: Marker;
  options: MapOptions;
  show$: Observable<boolean>; // = false;

  constructor() {}

  ngOnInit() {
    this.options = {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        })
      ],
      zoom: 18,
      center: [this.tournament.location.geoPos[1], this.tournament.location.geoPos[0]]
    };
    this.marker = marker([this.tournament.location.geoPos[1], this.tournament.location.geoPos[0]], {
      icon: icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
      })
    }).bindPopup('<p>' + this.tournament.description + '</p>');
    this.show$ = of(this.tournament.location !== undefined).pipe(delay(0));
  }

  onMapReady(map: L.Map) {
    // Do stuff with map
    const options = {
      url: 'https://pelias.gpulido.com/v1',
      params: { lang: 'es' },
      fullWidth: 650,
      expanded: true,
      markers: {
        icon: icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          popupAnchor: [2, -35],
          iconUrl: 'assets/marker-icon.png',
          shadowUrl: 'assets/marker-shadow.png'
        }),
        title: 'test',
        zIndexOffset: 1000
      }
    };
  }
}
