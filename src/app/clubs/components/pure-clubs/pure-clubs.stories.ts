import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PureClubsComponent } from './pure-clubs.component';
import { PureClubItemComponent } from '../pure-club-item/pure-club-item.component';
import { defaultClubs } from '../../clubs.stories';
import { MdcIconRegistry } from '@angular-mdc/web/icon';

export const clubs = defaultClubs;

const props = {
  //viewTournament: actions.viewTournament
};

export default {
  title: 'Clubs/List',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureClubsComponent, PureClubItemComponent],
      imports: [MaterialModule, FlexLayoutModule],
      providers: [MdcIconRegistry],
    }),
  ],

  excludeStories: ['clubs'],
};

export const Default = () => ({
  component: PureClubsComponent,
  props: {
    ...props,
    clubs: object('clubs', clubs),
    loading: false,
  },
});

Default.story = {
  name: 'default',
};
