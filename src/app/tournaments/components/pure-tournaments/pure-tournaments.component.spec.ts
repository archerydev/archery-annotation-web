import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureTournamentsComponent } from './pure-tournaments.component';
import { Tournament } from '../../model/tournament';
import { setupTournaments } from '../../model/tournament-data';
import { PureTournamentsCardListComponent } from '../pure-tournaments-card-list/pure-tournaments-card-list.component';

export const defaultTournaments: Tournament[] = setupTournaments();

describe('PureTournamentsComponent', () => {
  let component: PureTournamentsComponent;
  let fixture: ComponentFixture<PureTournamentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureTournamentsComponent , PureTournamentsCardListComponent]
    })
    .overrideComponent(PureTournamentsCardListComponent, {set: {
      template: '<div></div>'
    }})
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureTournamentsComponent);
    component = fixture.componentInstance;
    component.tournaments = defaultTournaments;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
