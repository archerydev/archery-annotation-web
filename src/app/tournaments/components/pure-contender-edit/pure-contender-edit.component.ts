import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { Tournament, Contender } from '../../model/tournament';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { MdcChipSetChange } from '@angular-mdc/web/chips';
import { Club } from '../../../clubs/model/club';

@Component({
  selector: 'app-pure-contender-edit',
  templateUrl: './pure-contender-edit.component.html',
  styleUrls: ['./pure-contender-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureContenderEditComponent implements OnInit, OnChanges {
  form: FormGroup;
  @Input() tournament: Tournament;
  @Input() contender: Contender;
  @Input() clubs: Club[];
  filteredClubs: Club[];
  @Input() mode: 'create' | 'update';
  title: string;

  @Output() saveContender: EventEmitter<Contender> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  constructor(private fb: FormBuilder) {
    const formControls = {
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, Validators.email, this.emailMatchTextValidator.bind(this)]],
      club: [null, [Validators.required]],
      archerLicense: ['', [Validators.required]],
      category: ['', [Validators.required]]
    };
    this.form = this.fb.group(formControls);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.contender) {
      this.form.patchValue({
        ...this.contender,
      });
      console.log(this.form.controls.club.value);
      if (this.contender && this.contender.email) {
        this.form.patchValue({
          confirmEmail: this.contender.email
        });
      }
    }
    if (changes.tournament) {
      if (this.mode === 'create') {
        this.title = 'Subscribe to ' + this.tournament?.description;
      } else {
        this.title = 'Edit Subscription to ' + this.tournament?.description;
      }
    }
  }

  ngOnInit() {
  }

  setEmailErrorConfirm() {
    const confirmEmail = this.form.controls['confirmEmail'];
    return confirmEmail.errors && confirmEmail.errors.email;
  }

  setMismatchEmailConfirm() {
    const confirmEmail = this.form.controls['confirmEmail'];
    return confirmEmail.errors && !confirmEmail.errors.email && confirmEmail.errors.mismatch;
  }
  setCategoryError() {}

  emailMatchTextValidator(fc: FormControl) {
    if (!this.form) {
      return null;
    }
    return fc.value === this.form.controls['email'].value ? null : { mismatch: true };
  }

  onCategorySetChange(evt: MdcChipSetChange): void {
    this.form.patchValue({
      category: evt.value.value
    });
  }

  onSave() {
    this.contender = { ...this.form.value, tournamentId: this.tournament.id } as Contender;
    delete this.contender['confirmEmail'];
    this.saveContender.emit(this.contender);
  }

  onClose() {
    this.cancel.emit();
  }

  onAutocompleteItemSelected(evt:any) {
    console.log(evt);
  }

  onFilterText(text:string) {
    this.filteredClubs = this.clubs.filter(c => c.name.toUpperCase().includes(text.toUpperCase()));
  }
}
