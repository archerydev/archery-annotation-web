import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClubEditComponent } from './containers/club-edit/club-edit.component';
import { ClubsComponent } from './containers/clubs/clubs.component';
import { ClubsResolver } from './services/clubs.resolver';

const routes: Routes = [
  {
    path: '',
    component: ClubsComponent,
    // resolve: {
    //   clubs: ClubsResolver
    // }
  },
  { path: ':clubId', component: ClubEditComponent },
  { path: 'new', component: ClubEditComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClubsRoutingModule {}
