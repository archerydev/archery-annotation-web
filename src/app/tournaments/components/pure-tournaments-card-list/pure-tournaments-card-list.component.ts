import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Tournament } from '../../model/tournament';
import { TournamentEntityService } from '../../services/tournament-entity.service';


@Component({
  selector: 'pure-tournaments-card-list',
  templateUrl: './pure-tournaments-card-list.component.html',
  styleUrls: ['./pure-tournaments-card-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PureTournamentsCardListComponent implements OnInit {

  @Input() tournaments: Tournament[] = [];

  @Input() loading = false;

  @Output() viewTournament: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  

}
