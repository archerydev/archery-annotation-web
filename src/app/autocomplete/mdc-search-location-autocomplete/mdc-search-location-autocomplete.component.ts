import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  AfterViewInit,
  OnDestroy,
  Injector
} from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { GeoLocation, GeocodingFeatureProperties, GeocodingGeometry, PlaceSuggestion } from '../model/geolocation';
import { Marker, MapOptions, tileLayer, marker, icon, Layer, latLng, LatLng } from 'leaflet';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import * as L from 'leaflet';
import { provideValueAccessor, FormControlSuperclass } from 's-ng-utils';

@Component({
  selector: 'mdc-search-location-autocomplete',
  templateUrl: './mdc-search-location-autocomplete.component.html',
  styleUrls: ['./mdc-search-location-autocomplete.component.scss'],
  providers: [provideValueAccessor(MdcSearchLocationAutocompleteComponent)],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdcSearchLocationAutocompleteComponent extends FormControlSuperclass<GeoLocation>
  implements OnInit, AfterViewInit, OnDestroy {
  @Input()
  searchAddressLabel = 'Search Address';

  @Input()
  streetNameLabel = 'Street';

  @Input()
  streetNumberLabel = 'Nr.';

  @Input()
  postalCodeLabel = 'Postal Code';

  @Input()
  localityLabel = 'City';

  @Input()
  regionLabel = 'Province';

  @Input()
  country: string | string[];

  @Output()
  onGeoLocationMapped: EventEmitter<GeoLocation> = new EventEmitter<GeoLocation>();

  @Input()
  itemText: string;

  @Input()
  geoLocation: GeoLocation;

  addressFormGroup: FormGroup;

  marker: Marker;
  options: MapOptions;
  private requestSub: Subscription;
  markers: Layer[] = [];
  center = latLng(46.879966, -121.726909);

  constructor(injector: Injector, private formBuilder: FormBuilder, private http: HttpClient) {
    super(injector);
  }

  handleIncomingValue(value: GeoLocation): void {
    this.geoLocation = value;
  }

  ngAfterViewInit(): void {
    this.syncAutoComplete(this.geoLocation);
  }

  ngOnInit() {
    this.addressFormGroup = this.createAddressFormGroup();
    this.options = {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        })
      ],
      zoom: 18,
      center: this.center
      // center: [this.geoLocation?.geoPos[1], this.geoLocation?.geoPos[0]]
    };
  }

  addMarker() {
    this.removeMarker();
    console.log(this.geoLocation);
    const newMarker = marker([this.geoLocation?.geoPos[1], this.geoLocation?.geoPos[0]], {
      icon: icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        popupAnchor: [2, -35],
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
      })
    }).bindPopup('<p>' + this.geoLocation?.description + '</p>');
    this.markers.push(newMarker);
    this.center = newMarker.getLatLng();
    newMarker.openPopup();
  }

  removeMarker() {
    this.markers.pop();
  }

  createAddressFormGroup(): FormGroup {
    return this.formBuilder.group({
      description: [null, Validators.required],
      streetName: [null, Validators.required],
      streetNumber: [null, Validators.required],
      postalCode: [null, Validators.required],
      city: [null, Validators.required],
      region: [null, Validators.required]
    });
  }
  selectedItem($event: PlaceSuggestion) {
    this.syncAutoComplete(this.parsePlace($event));
  }

  syncAutoComplete(geoLocation: GeoLocation) {
    console.log(geoLocation);
    if (this.geoLocation) {
      this.addressFormGroup.reset();
    }
    //TODO: check that the place has an address
    this.addressFormGroup.reset();
    this.geoLocation = geoLocation;

    if (this.geoLocation?.description) {
      this.addressFormGroup.get('description').patchValue(this.geoLocation.description);
    }
    if (this.geoLocation?.streetName) {
      this.addressFormGroup.get('streetName').patchValue(this.geoLocation.streetName);
    }
    if (this.geoLocation?.streetNumber) {
      this.addressFormGroup.get('streetNumber').patchValue(this.geoLocation.streetNumber);
    }
    if (this.geoLocation?.postalCode) {
      this.addressFormGroup.get('postalCode').patchValue(this.geoLocation.postalCode);
    }
    if (this.geoLocation?.region) {
      this.addressFormGroup.get('region').patchValue(this.geoLocation.region);
    }
    if (this.geoLocation?.city) {
      this.addressFormGroup.get('city').patchValue(this.geoLocation.city);
    }
    if (this.geoLocation?.geoPos) {
      this.addMarker();
    }
    this.onGeoLocationMapped.emit(this.geoLocation);
    this.emitOutgoingValue(this.geoLocation);
    this.itemText = this.geoLocation?.description;
    console.log(this.addressFormGroup.getRawValue());
  }

  parsePlace(place: PlaceSuggestion): GeoLocation {
    console.log(place);
    if (place.data.confidence < 0.8) {
      return {
        description: `LatLong: ${place.geometry.coordinates[1]}, ${place.geometry.coordinates[0]} near ${place.data.label}`,
        geoPos: place.geometry.coordinates
      };
    }
    return {
      description: place.data.label,
      geoPos: place.geometry.coordinates,
      streetName: place.data.street,
      streetNumber: place.data.housenumber,
      postalCode: place.data.postalcode,
      city: place.data.localadmin ?? place.data.locality,
      region: place.data.region
    };
  }
  onMapReady(map: L.Map) {
    // Do stuff with map
    // const options = {
    //   url: 'https://pelias.gpulido.com/v1',
    //   params: { lang: 'es' },
    //   fullWidth: 650,
    //   expanded: true,
    //   markers: {
    //     icon: icon({
    //       iconSize: [25, 41],
    //       iconAnchor: [13, 41],
    //       popupAnchor: [2, -35],
    //       iconUrl: 'assets/marker-icon.png',
    //       shadowUrl: 'assets/marker-shadow.png'
    //     }),
    //     title: 'test',
    //     zIndexOffset: 1000
    //   }
    // };
    // const geocoder = L.control.geocoder(options).addTo(map);
    // const geocoderEl = geocoder._container;
    // geocoderEl.parentNode.insertBefore(geocoderEl, geocoderEl.parentNode.childNodes[0]);
    // geocoder.focus();
    // geocoder.on('select', e => {
    //   console.log('You’ve selected', e.feature.properties);
    // });
  }
  onMapClick(event: any) {
    console.log(event);
    this.generateSuggestions(event.latlng);
  }

  private generateSuggestions(point: LatLng) {
    // const YOUR_API_KEY = '';
    const url = `https://pelias.gpulido.com/v1/reverse?point.lat=${point.lat}&point.lon=${point.lng}&size=1`;

    if (this.requestSub) {
      this.requestSub.unsubscribe();
    }
    this.requestSub = this.http.get(url).subscribe(
      (data: any) => {
        console.log(data);
        const placeSuggestions = data.features.map(feature => {
          const properties: GeocodingFeatureProperties = feature.properties as GeocodingFeatureProperties;
          const geometry: GeocodingGeometry = feature.geometry as GeocodingGeometry;
          return {
            fullAddress: properties.label, // this.generateFullAddress(properties),
            data: properties,
            geometry
          };
        });
        let ps = null;
        if (placeSuggestions.length) {
          ps = { ...placeSuggestions[0] };
          ps.geometry.coordinates = [point.lng, point.lat];
        }
        const geo = this.parsePlace(ps);
        this.syncAutoComplete(geo);
      },
      err => {
        console.log(err);
      }
    );
  }
}
