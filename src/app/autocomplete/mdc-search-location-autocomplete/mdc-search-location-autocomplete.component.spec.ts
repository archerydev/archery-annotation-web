import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdcSearchLocationAutocompleteComponent } from './mdc-search-location-autocomplete.component';

describe('MdcSearchLocationAutocompleteComponent', () => {
  let component: MdcSearchLocationAutocompleteComponent;
  let fixture: ComponentFixture<MdcSearchLocationAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdcSearchLocationAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdcSearchLocationAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
