import {
  Component,
  EventEmitter,
  Output,
  OnDestroy,
  Input,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  AfterViewInit,
  Injector,
  ViewChild
} from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MdcTextField } from '@angular-mdc/web/textfield';
import { provideValueAccessor, FormControlSuperclass } from 's-ng-utils';
import { MdcAutocompleteComponent } from '../mdc-autocomplete/mdc-autocomplete.component';
import { PlaceSuggestion, GeocodingFeatureProperties, GeocodingGeometry } from '../model/geolocation';

@Component({
  selector: 'mdc-location-autocomplete',
  templateUrl: './mdc-location-autocomplete.component.html',
  styleUrls: ['./mdc-location-autocomplete.component.scss'],
  providers: [provideValueAccessor(MdcLocationAutocompleteComponent)],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdcLocationAutocompleteComponent extends FormControlSuperclass<string>
  implements AfterViewInit, OnDestroy {
  @ViewChild(MdcAutocompleteComponent, { static: false }) _autocomplete!: MdcAutocompleteComponent<PlaceSuggestion>;
  @ViewChild(MdcTextField, { static: false }) _textField!: MdcTextField;
  @Input() item: string;
  @Input() country: string | string[] = 'ES';
  searchOptions: Subject<PlaceSuggestion[]> = new Subject<PlaceSuggestion[]>();

  private userInputTimeout: number;
  private requestSub: Subscription;

  @Output()
  locationChange: EventEmitter<PlaceSuggestion> = new EventEmitter<PlaceSuggestion>();

  locationSelected: boolean;
  filteredFn = (arg0: PlaceSuggestion) => arg0.fullAddress;

  ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
  }

  onChange(value: PlaceSuggestion) {
    console.log('selecteditem');
    this.locationSelected = true;
    this.locationChange.emit(value);
    this.emitOutgoingValue(value.data.label);
  }

  onFieldChange(value) {
    if (this.userInputTimeout) {
      window.clearTimeout(this.userInputTimeout);
    }
    if (!value || value.length < 3) {
      // do not need suggestions until for less than 3 letters
      this.searchOptions.next(null);
      return;
    }
    this.userInputTimeout = window.setTimeout(() => {
      this.generateSuggestions(value, false);
    }, 300);
  }

  onEnterPress(value: MdcTextField) {
    console.log('enter', value);
    if (!this.locationSelected) {
      this.generateSuggestions(value.value, true);
    }
    this.locationSelected = false;
  }

  clearQuery() {
    this._autocomplete.writeValue(null);
  }

  constructor(injector: Injector, private http: HttpClient) {
    super(injector);
  }

  ngAfterViewInit() {
    this._textField.writeValue(this.item);
    this.onFieldChange(this.item);
    this._autocomplete.hideOptions();
  }

  handleIncomingValue(value: string): void {
    this.item = value;
  }

  //TODO: parametrice boundaries, and lang
  private generateSuggestions(text: string, search: boolean) {
    // const YOUR_API_KEY = '';
    // const url = `https://api.geoapify.com/v1/geocode/api?q=${text}&limit=5&api_key=${YOUR_API_KEY}`;
    let url = `https://pelias.gpulido.com/v1/autocomplete?boundary.country=${this.country}&lang=es&text=${text}`;
    if (search) {
      url = `https://pelias.gpulido.com/v1/search?boundary.country=${this.country}&lang=es&text=${text}`;
    }

    console.log(url);
    if (this.requestSub) {
      this.requestSub.unsubscribe();
    }
    this.requestSub = this.http.get(url).subscribe(
      (data: GeoJSON.FeatureCollection) => {
        //console.log(data);
        const placeSuggestions = data.features.map(feature => {
          const properties: GeocodingFeatureProperties = feature.properties as GeocodingFeatureProperties;
          const geometry: GeocodingGeometry = feature.geometry as GeocodingGeometry;
          return {
            // shortAddress: this.generateShortAddress(properties),
            fullAddress: properties.label, // this.generateFullAddress(properties),
            data: properties,
            geometry
          };
        });
        this.searchOptions.next(placeSuggestions.length ? placeSuggestions : null);
      },
      err => {
        console.log(err);
      }
    );
  }
  // private generateShortAddress(properties: GeocodingFeatureProperties): string {
  //   let shortAddress = properties.name;
  //   if (!shortAddress && properties.street && properties.housenumber) {
  //     // name is not set for buildings
  //     shortAddress = `${properties.street} ${properties.housenumber}`;
  //   }
  //   shortAddress += properties.postcode && properties.city ? `, ${properties.postcode}-${properties.city}` : '';
  //   shortAddress +=
  //     !properties.postcode && properties.city && properties.city !== properties.name ? `, ${properties.city}` : '';
  //   shortAddress += properties.country && properties.country !== properties.name ? `, ${properties.country}` : '';
  //   return shortAddress;
  // }

  // private generateFullAddress(properties: GeocodingFeatureProperties): string {
  //   let fullAddress = properties.name;
  //   fullAddress += properties.street ? `, ${properties.street}` : '';
  //   fullAddress += properties.housenumber ? ` ${properties.housenumber}` : '';
  //   fullAddress += properties.postcode && properties.city ? `, ${properties.postcode}-${properties.city}` : '';
  //   fullAddress +=
  //     !properties.postcode && properties.city && properties.city !== properties.name ? `, ${properties.city}` : '';
  //   fullAddress += properties.postalcode ? ` ${properties.postalcode}` : '';
  //   fullAddress += properties.state ? `, ${properties.state}` : '';
  //   fullAddress += properties.country && properties.country !== properties.name ? `, ${properties.country}` : '';
  //   return fullAddress;
  // }
  getIcon(layer: string): string {
    if (layer === 'address') {
      return 'place';
    }
    return 'language';
  }
}
