import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { contender } from '../tournaments.stories';
import { PureContenderItemComponent } from './pure-contender-item.component';

const props = {};

export default {
  title: 'Contenders/List Item',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureContenderItemComponent],
      imports: [MaterialModule, FlexLayoutModule],
    }),
  ],
};

export const New = () => ({
  component: PureContenderItemComponent,
  props: {
    ...props,
    contender: object('contender', contender),
  },
});

New.story = {
  name: 'new',
};
