import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { GeoLocation, Club, emptyLocation } from '../../../clubs/model/club';
import { NgxRootFormComponent, DataInput, Controls } from 'ngx-sub-form';
import { Tournament, FileDesc } from '../../model/tournament';
import { FormControl, Validators } from '@angular/forms';
import { Time } from '@angular/common';
import { Category } from '../../model/category';
import { MailerDesc } from '../../../mailers/model/mailer';

export interface TournamentForm {
  id: string;
  description: string;
  longDescription: string;
  date: Date;
  hostingClub: Club;
  iconUrl: string;
  imageUrl: string;
  location: GeoLocation | null;
  price: number;
  categories: Category[];
  mailing: MailerDesc[];
  files: FileDesc[];
  _kuzzle_info: any;
}

@Component({
  selector: 'app-tournament-form',
  templateUrl: './tournament-form.component.html',
  styleUrls: ['./tournament-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TournamentFormComponent extends NgxRootFormComponent<Tournament, TournamentForm> {
  // tslint:disable-next-line:no-input-rename
  @DataInput()
  @Input('tournament')
  public dataInput: Required<Tournament> | null | undefined;

  @Input() allCategories: Category[];
  @Input() clubs: Club[];

  // tslint:disable-next-line:no-output-rename
  @Output('tournamentUpdated')
  public dataOutput: EventEmitter<Tournament> = new EventEmitter();

  @Output() public addMailer: EventEmitter<any> = new EventEmitter();
  @Output() public editMailer: EventEmitter<MailerDesc> = new EventEmitter();

  editorFocused: boolean;

  protected getFormControls(): Controls<TournamentForm> {
    return {
      id: new FormControl(null),
      description: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      longDescription: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      date: new FormControl(null, Validators.required),
      hostingClub: new FormControl(null, Validators.required),
      iconUrl: new FormControl(null),
      imageUrl: new FormControl(null),
      location: new FormControl(null),
      price: new FormControl(null),
      categories: new FormControl([], Validators.required),
      mailing: new FormControl(null),
      files: new FormControl(null),
      _kuzzle_info: new FormControl(null)
    };
  }
  public getDefaultValues(): Partial<TournamentForm> | null {
    return {
      id: null,
      description: null,
      longDescription: null,
      date: null,
      hostingClub: null,
      iconUrl: null,
      imageUrl: null,
      location: null,
      price: null,
      categories: null,
      files: null,
      mailing: null,
      _kuzzle_info: null
    };
  }

  protected transformToFormGroup(
    obj: Tournament | null,
    defaultValues: Partial<TournamentForm> | null
  ): TournamentForm | null {
    if (!obj) {
      return null;
    }
    let tourForm = {
      ...defaultValues,
      ...obj
    } as TournamentForm;
    return tourForm;
  }

  protected transformFromFormGroup(formValue: TournamentForm): Tournament | null {
    return {
      ...formValue,
      hostingClub: {
        id: formValue.hostingClub?.id,
        imageUrl: formValue.hostingClub?.imageUrl,
        name: formValue.hostingClub?.name
      }
    } as Tournament;
  }

  public onChangeLocation($event) {
    if ($event.checked) {
      this.formGroupControls.location.setValue({ ...emptyLocation });
    } else {
      this.formGroupControls.location.setValue(null);
    }
  }

  onAddMailer(): void {}

  onEditMailer(mailer: MailerDesc, idx: number) {}

  onAddFile(): void {
    // const dialogRef = this.dialog.open(PureFileEditComponent, this.getFileDialogConfig(emptyFile, 'create'));
  }

  onDeleteFile(file: FileDesc, idx: number) {
    console.log(file);
    //this.form.controls.files.value.splice(idx, 1);
  }

  onEditFile($event: any) {
    console.log('delete file', $event);
    // const dialogRef = this.dialog.open(PureFileEditComponent, this.getFileDialogConfig(file, 'update'));
  }
}
