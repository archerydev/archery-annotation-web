import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PureMailerTemplatePickerComponent } from './components/pure-mailer-template-picker/pure-mailer-template-picker.component';
import { MailerFormComponent } from './components/mailer-form/mailer-form.component';
import { MailerSubscriptionSubFormComponent } from './components/mailer-subscription-subform/mailer-subscription-subform.component';
import { MailerMailTemplateSubFormComponent } from './components/mailer-mail-template-subform/mailer-mail-template-subform.component';
import { MailerEditComponent } from './containers/mailer-edit/mailer-edit.component';
import { EntityMetadataMap, EntityDefinitionService, EntityDataService } from '@ngrx/data';
import { MailerEntityService } from './services/mailer-entity.service';
import { MailersDataService } from './services/mailers-data.service';
import { MailersComponent } from './containers/mailers/mailers.component';
import { MailersRoutingModule } from './mailers-routing.module';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';

const entityMetadada: EntityMetadataMap = {
  Mailer: {}
};

@NgModule({
  declarations: [
    PureMailerTemplatePickerComponent,
    MailerFormComponent,
    MailerSubscriptionSubFormComponent,
    MailerMailTemplateSubFormComponent,
    MailerEditComponent,
    MailersComponent
  ],
  imports: [CommonModule, MailersRoutingModule, MaterialModule, FlexLayoutModule, ReactiveFormsModule],
  providers: [MailerEntityService, MailersDataService],
  entryComponents: [PureMailerTemplatePickerComponent]
})
export class MailersModule {
  constructor(
    eds: EntityDefinitionService,
    entityDataService: EntityDataService,
    mailersDataService: MailersDataService
  ) {
    eds.registerMetadataMap(entityMetadada);
    entityDataService.registerService('Mailer', mailersDataService);
  }
}
