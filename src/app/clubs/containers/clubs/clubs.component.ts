import { Component, OnInit } from '@angular/core';
import { ClubEntityService } from '../../services/club-entity.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Club } from '../../model/club';

@Component({
  selector: 'app-clubs',
  templateUrl: './clubs.component.html',
  styleUrls: ['./clubs.component.scss']
})
export class ClubsComponent implements OnInit {
  constructor(private clubsService: ClubEntityService, private router: Router) {}

  activeClubs$: Observable<Club[]>;
  loading$: Observable<boolean>;

  ngOnInit() {
    this.reload();
  }

  reload() {
    this.activeClubs$ = this.clubsService.entities$;
    this.loading$ = this.clubsService.loading$;
  }

  onEditClub(event: Club) {
    console.log(event);
    this.router.navigate(['clubs', event.id]);
  }
  onNewClub() {
    this.router.navigate(['clubs', 'new']);
  }
}
