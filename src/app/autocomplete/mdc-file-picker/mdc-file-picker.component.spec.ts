import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdcFilePickerComponent } from './mdc-file-picker.component';

describe('MdcFilePickerComponent', () => {
  let component: MdcFilePickerComponent;
  let fixture: ComponentFixture<MdcFilePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdcFilePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdcFilePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
