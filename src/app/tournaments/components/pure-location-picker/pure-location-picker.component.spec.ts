import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureLocationPickerComponent } from './pure-location-picker.component';

describe('PureLocationPickerComponent', () => {
  let component: PureLocationPickerComponent;
  let fixture: ComponentFixture<PureLocationPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureLocationPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureLocationPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
