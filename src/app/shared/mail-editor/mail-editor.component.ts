import { Component, OnInit } from '@angular/core';
import grapesjs from 'grapesjs';
import grapesJSMJML from 'grapesjs-mjml'

@Component({
  selector: 'app-mail-editor',
  templateUrl: './mail-editor.component.html',
  styleUrls: ['./mail-editor.component.scss']
})
export class MailEditorComponent implements OnInit {

  private _editor: any;
  constructor() { }

  ngOnInit() {
    this._editor = this.initializeEditor();

  }
  private initializeEditor(): any {
    return grapesjs.init({
      container: '#gjs',
      components: '<mjml> <mj-body></mj-body></mjml>',
      //fromElement: true,
      // Size of the editor
      // height: '300px',
      // width: 'auto',
      avoidInlineStyle : false,
      plugins: [grapesJSMJML],
      // Disable the storage manager for the moment
      storageManager: { type: null },
      // Avoid any default panel
      panels: {},
    });

  }


}
