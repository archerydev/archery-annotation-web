import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureContenderEditComponent } from './pure-contender-edit.component';

describe('PureContenderEditComponent', () => {
  let component: PureContenderEditComponent;
  let fixture: ComponentFixture<PureContenderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureContenderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureContenderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
