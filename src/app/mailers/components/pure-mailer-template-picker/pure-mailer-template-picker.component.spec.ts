import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureMailerTemplatePickerComponent } from './pure-mailer-template-picker.component';

describe('PureMailerTemplatePickerComponent', () => {
  let component: PureMailerTemplatePickerComponent;
  let fixture: ComponentFixture<PureMailerTemplatePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureMailerTemplatePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureMailerTemplatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
