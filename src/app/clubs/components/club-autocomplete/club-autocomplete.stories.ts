import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';

import { MdcAutocompleteComponent } from '../../../autocomplete/mdc-autocomplete/mdc-autocomplete.component';
import { HttpClientModule } from '@angular/common/http';
import { ClubAutocompleteComponent } from './club-autocomplete.component';
import { defaultClubs, club } from 'src/app/tournaments/components/tournaments.stories';
import { MdcMenuModule } from '@angular-mdc/web/menu';
import { MdcTextFieldModule } from '@angular-mdc/web/textfield';
import { MdcListModule } from '@angular-mdc/web/list';
import { ErrorStateMatcher } from '@angular-mdc/web/form-field';

export default {
  title: 'Clubs/Autocomplete',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [ClubAutocompleteComponent, MdcAutocompleteComponent],
      imports: [MdcMenuModule, MdcTextFieldModule, MdcListModule, HttpClientModule],
      providers: [ErrorStateMatcher],
    }),
  ],
};

export const NewEmpty = () => ({
  component: ClubAutocompleteComponent,
  props: {
    items: object('items', defaultClubs),
    item: object('item', club),
    // propertySelector: object('propertySelector', 'name')
  }
 });

NewEmpty.story = {
  name: 'new empty',
};

export const EmptyList = () => ({
  component: ClubAutocompleteComponent,
  props: {
    items: object('items', null),
    item: object('item', club),
  }
 });

 EmptyList.story = {
  name: 'empty list',
};

