import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { PureTournamentsCardListComponent } from './pure-tournaments-card-list.component';
import { PureTournamentCardComponent } from '../pure-tournament-card/pure-tournament-card.component';
import { MaterialModule } from 'src/app/material.module';
import { defaultTournaments, tourActions } from '../tournaments.stories';

const props = {
  viewTournament: tourActions.viewTournament,
};

export default {
  title: 'Tournaments/Card List',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureTournamentsCardListComponent, PureTournamentCardComponent],
      imports: [MaterialModule],
    }),
  ],
};

export const Default = () => ({
  component: PureTournamentsCardListComponent,
  props: {
    ...props,
    tournaments: object('tournaments', defaultTournaments),
  },
});

Default.story = {
  name: 'default',
};

export const Loading = () => ({
  component: PureTournamentsCardListComponent,
  props: {
    ...props,
    tournaments: [],
    loading: true,
  },
});

Loading.story = {
  name: 'loading',
};

export const Empty = () => ({
  component: PureTournamentsCardListComponent,
  props: {
    ...props,
    tournaments: [],
    loading: false,
  },
});

Empty.story = {
  name: 'empty',
};
