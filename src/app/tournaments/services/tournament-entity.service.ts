import { Injectable } from '@angular/core';
import { EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Tournament } from '../model/tournament';
import { KuzzleRealtimeEntityService } from 'kuzzle-ngrx';
import { KuzzleService } from 'kuzzle-ngrx';

@Injectable()
export class TournamentEntityService extends KuzzleRealtimeEntityService<Tournament> {
  constructor(kuzzle: KuzzleService, serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Tournament', kuzzle, serviceElementsFactory);
  }
}
