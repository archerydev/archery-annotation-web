import { moduleMetadata } from '@storybook/angular';
import { object, withKnobs } from '@storybook/addon-knobs';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PureClubItemComponent } from './pure-club-item.component';
import { club, clubActions } from '../../clubs.stories';

const props = {
  
};

export default {
  title: 'Clubs/List Item',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureClubItemComponent],
      imports: [MaterialModule, FlexLayoutModule],
    }),
  ],
};

export const Default = () => ({
  component: PureClubItemComponent,
  props: {
    ...props,
    club: object('club', club),
  },
});

Default.story = {
  name: 'default',
};
