import { moduleMetadata } from '@storybook/angular';
import { withKnobs } from '@storybook/addon-knobs';
import { PureClubPickerComponent } from './pure-club-picker.component';
import { MaterialModule } from 'src/app/material.module';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';
import { clubActions, MdcDialogRefFactory, defaultClubs, club } from '../../clubs.stories';

const props = {
  selectClub: clubActions.selectClub,
};

export default {
  title: 'Clubs/Picker',

  decorators: [
    withKnobs,
    moduleMetadata({
      declarations: [PureClubPickerComponent],
      imports: [MaterialModule],
      entryComponents: [PureClubPickerComponent],
      providers: [
        {
          provide: MdcDialogRef,
          useFactory: MdcDialogRefFactory,
          deps: [MDC_DIALOG_DATA],
        },
      ],
    }),
  ],
};

export const Default = () => ({
  component: PureClubPickerComponent,
  props: {
    ...props,
  },
  moduleMetadata: {
    providers: [
      {
        provide: MDC_DIALOG_DATA,
        useValue: {
          clubs: defaultClubs,
          loading: false,
          selectedClub: club,
        },
      },
    ],
  },
});

Default.story = {
  name: 'default',
};

export const Selected = () => ({
  component: PureClubPickerComponent,
  props: {
    ...props,
  },
  moduleMetadata: {
    providers: [
      {
        provide: MDC_DIALOG_DATA,
        useValue: {
          clubs: defaultClubs,
          loading: false,
          selectedClub: club,
        },
      },
    ],
  },
});

Selected.story = {
  name: 'selected',
};

export const Loading = () => ({
  component: PureClubPickerComponent,
  props: {
    ...props,
  },
  moduleMetadata: {
    providers: [
      {
        provide: MDC_DIALOG_DATA,
        useValue: {
          clubs: [],
          loading: true,
          selectedClub: null,
        },
      },
    ],
  },
});

Loading.story = {
  name: 'loading',
};

export const Empty = () => ({
  component: PureClubPickerComponent,
  props: {
    ...props,
  },
  moduleMetadata: {
    providers: [
      {
        provide: MDC_DIALOG_DATA,
        useValue: {
          clubs: [],
          loading: false,
          selectedClub: null,
        },
      },
    ],
  },
});

Empty.story = {
  name: 'empty',
};
