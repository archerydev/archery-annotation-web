import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityMetadataMap, EntityDefinitionService, EntityDataService } from '@ngrx/data';
import { PureClubItemComponent } from './components/pure-club-item/pure-club-item.component';
import { PureClubsComponent } from './components/pure-clubs/pure-clubs.component';
import { ClubEditComponent } from './containers/club-edit/club-edit.component';
import { PureClubPickerComponent } from './components/pure-club-picker/pure-club-picker.component';
import { ClubFormComponent } from './components/club-form/club-form.component';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ReactiveFormsModule } from '@angular/forms';
import { AutocompleteModule } from '../autocomplete/autocomplete.module';
import { HttpClientModule } from '@angular/common/http';
import { ClubEntityService } from './services/club-entity.service';
import { ClubsDataService } from './services/clubs-data.service';
import { ClubsComponent } from './containers/clubs/clubs.component';
import { ClubsRoutingModule } from './clubs-routing.module';
import { ClubAutocompleteComponent } from './components/club-autocomplete/club-autocomplete.component';
import { GeolocationSubFormComponent } from './components/geolocation-sub-form/geolocation-sub-form.component';
import { ClubsResolver } from './services/clubs.resolver';


const entityMetadada: EntityMetadataMap = {
  Club: {},
};

@NgModule({
  declarations: [
    PureClubItemComponent,
    PureClubsComponent,
    ClubEditComponent,
    PureClubPickerComponent,
    ClubFormComponent,
    ClubsComponent,
    ClubAutocompleteComponent,
    GeolocationSubFormComponent,
  ],
  imports: [
    CommonModule,
    ClubsRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    LeafletModule,
    AutocompleteModule,
    HttpClientModule,
  ],
  exports: [
    ClubAutocompleteComponent,
    GeolocationSubFormComponent,
  ],
  providers: [
    ClubEntityService,
    ClubsDataService,
    ClubsResolver
  ],
  entryComponents: [
    ClubEditComponent,
    PureClubPickerComponent,
  ]

})
export class ClubsModule {
  constructor(
    eds: EntityDefinitionService,
    entityDataService: EntityDataService,
    clubsDataService: ClubsDataService,
  ) {
    eds.registerMetadataMap(entityMetadada);
    entityDataService.registerService('Club', clubsDataService);
  }

 }
