import { Injectable } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web/snackbar';

@Injectable({
    providedIn: 'root'
  })
export class UIService {

  constructor(private snackbar: MdcSnackbar) {}

  public showSnackbar(message, action) {
    this.snackbar.open(message, action);
  }
}
