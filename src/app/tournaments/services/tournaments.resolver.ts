import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap, filter, first } from 'rxjs/operators';
import { TournamentEntityService } from './tournament-entity.service';

@Injectable()
export class TournamentsResolver implements Resolve<boolean> {
  constructor(private tournamentsService: TournamentEntityService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.tournamentsService.loaded$.pipe(
      tap(loaded => {
        if (!loaded) {
          this.tournamentsService.getAll();
        }
      }),
      filter(loaded => !!loaded),
      first()
    );
  }
}
