import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, resultMemoize } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, concatMap, map, takeUntil, tap, switchMap, mergeMap } from 'rxjs/operators';
import { serializeError } from 'serialize-error';

import * as fromFileUploadActions from './actions';
import { KuzzleFilesService } from 'kuzzle-ngrx';

@Injectable()
export class UploadFileEffects {
  @Effect()
  uploadRequestEffect$: Observable<Action> = this.actions$.pipe(
    ofType(fromFileUploadActions.ActionTypes.UPLOAD_REQUEST),
    concatMap(action =>
      this.fileUploadService.getUploadUrl(action.payload.file, action.payload.collection).pipe(
        tap(a => console.log(a)),
        switchMap(r =>
          this.fileUploadService.uploadFile(action.payload.file, r.result.uploadUrl).pipe(
            takeUntil(this.actions$.pipe(ofType(fromFileUploadActions.ActionTypes.UPLOAD_CANCEL))),
            map(event => this.getActionFromHttpEvent(event, r.result.fileKey)),
            catchError(error => of(this.handleError(error)))
          )
        )
      )
    )
  );

  @Effect()
  validateFileUploadedEffect$: Observable<Action> = this.actions$.pipe(
    ofType(fromFileUploadActions.ActionTypes.UPLOAD_COMPLETED),
    concatMap(a =>
      this.fileUploadService.validateFile(a.payload.file_key).pipe(
        tap(e => console.log(e)),
        map(e => new fromFileUploadActions.ValidatedCompletedAction({ file_path: e.result.fileUrl })),
        catchError(error => of(this.handleError(error)))
      )
    )
  );

  constructor(
    private fileUploadService: KuzzleFilesService,
    private actions$: Actions<fromFileUploadActions.Actions>
  ) {}

  private getActionFromHttpEvent(event: HttpEvent<any>, fileKey: string) {
    switch (event.type) {
      case HttpEventType.Sent: {
        return new fromFileUploadActions.UploadStartedAction();
      }
      case HttpEventType.UploadProgress: {
        return new fromFileUploadActions.UploadProgressAction({
          progress: Math.round((100 * event.loaded) / event.total)
        });
      }
      case HttpEventType.ResponseHeader: {
        if (event.status === 200) {
          return new fromFileUploadActions.UploadCompletedAction({ file_key: '' });
        } else {
          return new fromFileUploadActions.UploadFailureAction({
            error: event.statusText
          });
        }
      }
      case HttpEventType.Response: {
        if (event.status === 200) {
          return new fromFileUploadActions.UploadCompletedAction({ file_key: fileKey });
        } else {
          return new fromFileUploadActions.UploadFailureAction({
            error: event.statusText
          });
        }
      }
      default: {
        return new fromFileUploadActions.UploadFailureAction({
          error: `Unknown Event: ${JSON.stringify(event)}`
        });
      }
    }
  }

  private handleError(error: any) {
    const friendlyErrorMessage = serializeError(error).message;
    return new fromFileUploadActions.UploadFailureAction({
      error: friendlyErrorMessage
    });
  }
}
