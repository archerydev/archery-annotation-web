import { MailerTemplate, DataSource, Mailer } from './mailer';

export const DataSources: DataSource[] = [
  {
    description: 'Get the Contender\'s tournament',
    collection: 'tournament',
    query: '{ "match": { "_id": "{{contender.tournamentId}}" }}',
    relationType: '1-1'
  },

]

export const MAILTER_TEMPLATES: any = {
  1: {
    id: 1,
    description: 'Send mail on contender subscription',
    mailTemplate: {
      from: '{{tournament.email}}',
      to: '{{contender.email}}',
      subject: 'Registro en el {{tournament.name}}',
      body:
        `Estimado {{contender.name}}:\

        Confirmamos tu inscripción en el {{tournament.name}}, al tiempo que agradecemos tu participación.
        Si surge alguna circunstancia que te impidiera acudir a la competición, rogamos nos lo hagas saber, al objeto e poder reasignar tu plaza a otro competidor que pudiera estar en lista de espera.
        Para ello, puedes utilizar el link que figura al pie de tus datos de inscripción adjuntos.
        Un cordial saludo.`
    },
    subscription: {
      collection: 'Contender',
      filters: '{ "equals": { "tournamentId": "{{masterId}}" } }',
      notificationAction: 'create',
      notificationType: 'document'
    },
    datasources: DataSources
  },
  2: {
    id: 2,
    description: 'Send mail on contender asignation',
    mailTemplate: {
      from: '{{tournament.email}}',
      to: '{{contender.email}}',
      subject: 'Asignación de dianas en el {{tournament.name}}',
      body:
        `Estimado {{contender.name}}:\

        Una vez efectuada la asignación de dianas, que ya se encuentra disponible en nuestra web, rogamos tomes nota de que te ha correspondido el puesto {{contender.slot}}.

        Así pues, te esperamos el próximo {{tournament.date}}.

        Un cordial saludo.`
    },
    subscription: {
      collection: 'Contender',
      filters: '{ "and": [ { "equals": { "tournamentId": "{{masterId}}" } }, {"equals": {"state": "assigned"}} ] }',
      notificationAction: 'update',
      notificationType: 'document'
    },
    datasources: DataSources
  }
};

export const MAILERS: any = {
  1: {
    id: '1',
    masterCollection: 'tournament',
    masterId: '2',
    description: 'Send mail on contender subscription',
    mailTemplate: {
      from: '{{tournament.email}}',
      to: '{{contender.email}}',
      subject: 'Registro en el {{tournament.name}}',
      body:
        `Estimado {{contender.name}}:\

        Confirmamos tu inscripción en el {{tournament.name}}, al tiempo que agradecemos tu participación.
        Si surge alguna circunstancia que te impidiera acudir a la competición, rogamos nos lo hagas saber, al objeto e poder reasignar tu plaza a otro competidor que pudiera estar en lista de espera.
        Para ello, puedes utilizar el link que figura al pie de tus datos de inscripción adjuntos.
        Un cordial saludo.`
    },
    subscription: {
      collection: 'Contender',
      filters: '{ "equals": { "tournamentId": "2" } }',
      notificationAction: 'create',
      notificationType: 'document'
    },
    datasources: DataSources
  },
  2: {
    id: '2',
    masterCollection: 'tournament',
    masterId: '2',
    description: 'Send mail on contender asignation',
    mailTemplate: {
      from: '{{tournament.email}}',
      to: '{{contender.email}}',
      subject: 'Asignación de dianas en el {{tournament.name}}',
      body:
        `Estimado {{contender.name}}:\

        Una vez efectuada la asignación de dianas, que ya se encuentra disponible en nuestra web, rogamos tomes nota de que te ha correspondido el puesto {{contender.slot}}.

        Así pues, te esperamos el próximo {{tournament.date}}.

        Un cordial saludo.`
    },
    subscription: {
      collection: 'Contender',
      filters: '{ "and": [ { "equals": { "tournamentId": "2" } }, { "equals": { "state": "assigned"}} ] }',
      notificationAction: 'update',
      notificationType: 'document'
    },
    datasources: DataSources
  }
};

export function setupMailerTemplates(): MailerTemplate[] {
  return Object.values(MAILTER_TEMPLATES).map(t => ({
    ...(t as object)
  })) as MailerTemplate[];
}

export function setupMailers(): Mailer[] {
  return Object.values(MAILERS).map(t => ({
    ...(t as object)
  })) as Mailer[];
}


export const Collections = ['Contender'];

export const Actions = ['create', 'delete', 'update'];

export const Types = ['document'];

