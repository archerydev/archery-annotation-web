import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { NgxSubFormComponent, subformComponentProviders, Controls } from 'ngx-sub-form';
import { Subscription } from '../../model/mailer';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-mailer-subscription-subform',
  templateUrl: './mailer-subscription-subform.component.html',
  styleUrls: ['./mailer-subscription-subform.component.scss'],
  providers: subformComponentProviders(MailerSubscriptionSubFormComponent),
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MailerSubscriptionSubFormComponent extends NgxSubFormComponent<Subscription> {

  @Input() collections: string[];
  @Input() actions: string[];
  @Input() types: string[];
  protected getFormControls(): Controls<Subscription> {
    return {
        collection: new FormControl(null, Validators.required),
        filters: new FormControl(null, Validators.required),
        notificationAction: new FormControl(null, Validators.required),
        notificationType: new FormControl(null, Validators.required)
    };
  }
  public getDefaultValues(): Partial<Subscription> | null {
    return {
      notificationType: 'document',
    };
  }




}
