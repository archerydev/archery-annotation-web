import {
  Component,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  AfterViewInit,
  Injector,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import { provideValueAccessor, FormControlSuperclass } from 's-ng-utils';
import { MdcDialog } from '@angular-mdc/web/dialog';
import { FileListDialogComponent } from 'src/upload-file/file-list-dialog.component';

@Component({
  selector: 'mdc-file-picker',
  templateUrl: './mdc-file-picker.component.html',
  styleUrls: ['./mdc-file-picker.component.scss'],
  providers: [provideValueAccessor(MdcFilePickerComponent)],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdcFilePickerComponent extends FormControlSuperclass<string> implements AfterViewInit {
  @Input() item: string;

  @Input() buttonCaption: string = "Pick Image";

  handleIncomingValue(value: string): void {
    this.item = value;
  }

  constructor(injector: Injector, private cd: ChangeDetectorRef, public dialog: MdcDialog) {
    super(injector);
  }

  ngAfterViewInit() {
    // this.onFieldChange(this.item);
  }

  onSelectImage() {
    const dialogRef = this.dialog.open(FileListDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null && result !== '') {
        const filePath = (result as { file_path: string }).file_path;
        this.item = filePath;
        this.emitOutgoingValue(this.item);
        this.cd.detectChanges();
      }
    });
  }
}
