import { Injectable } from '@angular/core';
import { Mailer } from '../model/mailer';
import { KuzzleDataService, KuzzleService } from 'kuzzle-ngrx';

@Injectable()
export class MailersDataService extends KuzzleDataService<Mailer> {
  constructor(kuzzle: KuzzleService) {
    super('Mailer', kuzzle);
  }
}
