import { Position } from 'geojson';

export interface GeoLocation {
  description: string;
  geoPos?: Position;
  streetName?: string;
  streetNumber?: string;
  postalCode?: string;
  city?: string;
  region?: string;
}


export interface PlaceSuggestion {
  // shortAddress: string;
  fullAddress: string;
  data: GeocodingFeatureProperties;
  geometry: GeocodingGeometry;
}

export interface GeocodingGeometry {
  coordinates: number[];
  type: string;
}
export interface GeocodingFeatureProperties {
  layer: string;
  label: string;
  name: string;
  country: string;
  macroregion: string;
  region: string;
  postalcode: string;
  street: string;
  housenumber: string;
  localadmin: string;
  locality: string;
  confidence: number;
}
