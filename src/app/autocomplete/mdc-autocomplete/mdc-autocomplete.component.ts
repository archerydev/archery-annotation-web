import {
  Component,
  OnInit,
  ContentChild,
  ViewChild,
  AfterViewInit,
  Input,
  Output,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  forwardRef,
  ElementRef,
  EventEmitter,
  HostListener
} from '@angular/core';
import { MdcMenu } from '@angular-mdc/web/menu';
import { MdcMenuSurfaceAnchor, AnchorMargin } from '@angular-mdc/web/menu-surface';
import { MdcTextField } from '@angular-mdc/web/textfield';
import { MdcList } from '@angular-mdc/web/list';
import { MdcTopAppBarFixedAdjust } from '@angular-mdc/web/top-app-bar';

import { DefaultFocusState } from '@material/menu';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Subject, Observable } from 'rxjs';

export enum KEY_CODE {
  UP_ARROW = 'ArrowUp',
  RIGHT_ARROW = 'ArrowRight',
  LEFT_ARROW = 'ArrowLeft',
  DOWN_ARROW = 'ArrowDown'
}

@Component({
  selector: 'mdc-autocomplete',
  templateUrl: './mdc-autocomplete.component.html',
  styleUrls: ['./mdc-autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MdcAutocompleteComponent),
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdcAutocompleteComponent<T> implements OnInit, AfterViewInit, ControlValueAccessor {
  constructor() {}

  @Input() filteredFn: (arg0: T) => string;
  @Input() filteredItems: Subject<T[]>;
  @Input() openOnClick = false;

  @ViewChild(MdcMenuSurfaceAnchor, { static: false }) _selectAnchor!: MdcMenuSurfaceAnchor;
  @ViewChild('autocomplete', { static: false }) _container!: ElementRef;
  @ContentChild(MdcTextField, { static: false }) _textField!: MdcTextField;
  @ContentChild(MdcMenu, { static: false }) _menu!: MdcMenu;
  @ContentChild(MdcList, { static: false }) _list!: MdcList;

  @Input() item: T;
  @Output()
  selectedItem: EventEmitter<T> = new EventEmitter<T>();

  filteredItemsCount: number;

  isDisabled: boolean;

  focusedList = false;
  focusedText = false;
  focused() {
    return this.focusedList || this.focusedText;
  }

  onChange = (_: any) => {};
  onTouch = () => {};

  writeValue(value: T): void {
    if (value) {
      this.item = value || null;
      if (this._textField) {
        this._syncTextField();
      }
    } else {
      this.item = null;
      if (this._textField) {
        this._textField.setValue('', false);
      }
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._textField.disabled = coerceBooleanProperty(isDisabled);
    this.isDisabled = isDisabled;
  }

  ngOnInit() {}

  _syncTextField() {
    if (this.item) {
      this._textField.value = this.filteredFn(this.item);
    }
  }
  _setItem(newItem: T) {
    this.hideOptions();
    this.writeValue(newItem);
    this.onChange(this.item);
    this.selectedItem.next(this.item);
  }

  ngAfterViewInit() {
    this._list.wrapFocus = true;
    this._list.singleSelection = true;
    // TODO: unsubscribe
    this.filteredItems.subscribe(values => {
      if (!values) {
        this.filteredItemsCount = 0;
        return;
      }
      this.filteredItemsCount = values.length;

      if (this._textField.value && this._textField.value !== '' && !this._menu.open && values.length > 0) {
        this.showOptions();
      }
    });

    this._textField._onFocus.subscribe((v: boolean) => {
      this.focusedText = v;
    });

    // TODO: ubsubscribe
    this._textField.blur.subscribe(() => {
      // the timeout is needed in case the blur is caused for selecting
      // a new item. As the blur is before selection we need to give the selection
      // a chance to change the value
      // In case the textfield is not longer selected and it is half written recover the
      // old value
      setTimeout(() => {
        this._syncTextField();
      }, 100);
    });
    this._menu.anchorElement = this._selectAnchor.elementRef.nativeElement;
    this._menu.anchorCorner = 'bottomStart';
    this._menu.anchorMargin = '{top: 10}' as AnchorMargin;
    this._menu.defaultFocusState = DefaultFocusState.NONE;
    this._menu.selected.asObservable().subscribe(item => {
      // If we don't set the timeout, if the call to setItem
      // ended on a change on the menu items it will throw an exception
      setTimeout(() => {
        this.focusedList = false;
        this._setItem(item.source.value);
      }, 100);
    });
    this._syncTextField();
  }

  onClick() {
    if (this.openOnClick && !this._menu.open && (!this.item || this.item === null) && this.filteredItemsCount > 0) {
      this.showOptions();
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    // Handle arrow to select menu
    if (!this._menu) {
      return;
    }
    if (!this.focused()) {
      return;
    }
    if (event.key === KEY_CODE.DOWN_ARROW || event.key === KEY_CODE.UP_ARROW) {
      if (this.focusedText) {
        if (!this._menu.open) {
          this.showOptions();
        } else {
          if (event.key === KEY_CODE.DOWN_ARROW) {
            this._list.focusFirstElement();
          } else {
            this._list.focusLastElement();
          }
          this.focusedList = true;
        }
        event.stopPropagation();
      }
    }
  }

  showOptions() {
    this._menu.open = true;
    this.focusedList = false;
  }
  hideOptions() {
    this._menu.open = false;
    this.focusedList = false;
  }
}
