import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromFileUploadActions from './store/actions';
import * as fromFileUploadSelectors from './store/selectors';
import * as fromFileUploadState from './store/state';
import { Observable, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web/dialog';

@Component({
  templateUrl: 'upload-file-dialog.component.html'
})
export class UploadFileDialogComponent implements OnInit, OnDestroy {
  completed$: Observable<boolean>;
  progress$: Observable<number>;
  error$: Observable<string>;
  filePath$: Observable<string>;

  isInProgress$: Observable<boolean>;
  isReady$: Observable<boolean>;
  hasFailed$: Observable<boolean>;
  multipleFiles: boolean;

  pathSubs: Subscription;
  uploadDir: string;

  constructor(
    public dialogRef: MdcDialogRef<UploadFileDialogComponent>,
    @Inject(MDC_DIALOG_DATA) data,
    private store$: Store<fromFileUploadState.State>
  ) {
    this.multipleFiles = data.mode !== 'singleFile';
    this.uploadDir = data.uploadDir;
    console.log(this.multipleFiles);
  }

  ngOnInit() {
    this.completed$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileCompleted));

    this.progress$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileProgress));

    this.error$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileError));

    this.isInProgress$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileInProgress));

    this.isReady$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileReady));

    this.hasFailed$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileFailed));

    this.filePath$ = this.store$.pipe(select(fromFileUploadSelectors.selectUploadFileCompletedPath));

    if (!this.multipleFiles) {
      this.pathSubs = this.store$
        .pipe(select(fromFileUploadSelectors.selectUploadFileCompletedPath))
        .pipe(
          tap(fp => console.log(fp)),
          filter(fp => fp !== null && fp !== '')
        )
        .subscribe(filepath => {
          console.log('filepath: ' + filepath);
          this.dialogRef.close({ file_path: filepath });
        });
    }
  }

  uploadFile(event: any) {
    const files: FileList = event.target.files;
    const file = files.item(0);

    this.store$.dispatch(
      new fromFileUploadActions.UploadRequestAction({
        file,
        collection:this.uploadDir
      })
    );

    // clear the input form
    event.srcElement.value = null;
  }

  ngOnDestroy(): void {
    console.log('destroy');
    if (this.pathSubs !== null) {
      this.pathSubs.unsubscribe();
    }
  }

  resetUpload() {
    this.store$.dispatch(new fromFileUploadActions.UploadResetAction());
  }

  cancelUpload() {
    this.store$.dispatch(new fromFileUploadActions.UploadCancelAction());
  }
}
